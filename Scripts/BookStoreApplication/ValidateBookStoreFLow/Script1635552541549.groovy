import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

WebUI.callTestCase(findTestCase('Login/login'), [:], FailureHandling.STOP_ON_FAILURE)

//validate that the Username of logged in user should be displayed on the Profile
WebUI.verifyElementText(findTestObject('Object Repository/BookStoreApplication/UserProfile/ProfileName'), GlobalVariable.Username)

//Validate the 'Add to collection' feature for added books
WebUI.waitForElementPresent(findTestObject('Object Repository/BookStoreApplication/UserProfile/SelectBook'),10)

WebUI.waitForElementClickable(findTestObject('Object Repository/BookStoreApplication/UserProfile/SelectBook'),10)

WebUI.click(findTestObject('Object Repository/BookStoreApplication/UserProfile/SelectBook'))

WebUI.waitForElementPresent(findTestObject('Object Repository/BookStoreApplication/UserProfile/AddCollection'),10)

WebUI.waitForElementClickable(findTestObject('Object Repository/BookStoreApplication/UserProfile/AddCollection'),10)

WebUI.click(findTestObject('Object Repository/BookStoreApplication/UserProfile/AddCollection'))

WebUI.delay(2)

//Validate the alert message for 'Add to collection' button
String alertText = WebUI.getAlertText()

WebUI.verifyMatch(alertText, 'Book already present in the your collection!', false)

WebUI.acceptAlert()

WebUI.scrollToElement(findTestObject('BookStoreApplication/UserProfile/Profile') , 10)

WebUI.waitForElementPresent(findTestObject('BookStoreApplication/UserProfile/Profile'),10)

WebUI.waitForElementClickable(findTestObject('BookStoreApplication/UserProfile/Profile'),10)

WebUI.scrollToElement(findTestObject('BookStoreApplication/UserProfile/Profile') , 10)

WebUI.click(findTestObject('BookStoreApplication/UserProfile/Profile'))

// Validae that added book is present in user book list
WebUI.waitForElementPresent(findTestObject('Object Repository/BookStoreApplication/BookStore/CollectionBooksList') , 10)

List<WebElement> booksList = WebUI.findWebElements(findTestObject('Object Repository/BookStoreApplication/BookStore/CollectionBooksList'),10)

List<String> bookName= new ArrayList<String>()

for (WebElement element : booksList)
{
	bookName.add(element.getText())
	
}

if(bookName.contains('Speaking JavaScript'))
	System.out.println('Book is successfully added to list')
else
	KeywordUtil.markFailed('Book is not added successfully')
	
