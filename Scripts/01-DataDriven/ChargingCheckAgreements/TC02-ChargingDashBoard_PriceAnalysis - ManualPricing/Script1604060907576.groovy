import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoin
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.sql.DriverManager as DriverManager
import java.sql.ResultSet as ResultSet
import java.sql.Statement as Statement
import com.kms.katalon.core.annotation.Keyword as Keyword
import java.text.SimpleDateFormat as Date
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

String Rowstart = 1

String Rowend = 50

List<List> ChargingOutput = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'('19pvi2ljFwnpnF86R1JZtDovgMQPoCR53TOsnwc5YABI', 
    (('ChargingOutput2!A' + Rowstart) + ':z') + Rowend)

List<List> ChargingOutputheader = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'('19pvi2ljFwnpnF86R1JZtDovgMQPoCR53TOsnwc5YABI',
	(('InvoiceDashboard!A' + Rowstart) + ':M') + Rowend)


this.RowNum = (this.RowNum - 1)


String inpService = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Testcase'))


String inpChargeQuantity = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Charge Quantity'))

String inpChargeUOM = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Charge UoM'))


String inpPricingType = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Pricing Type'))


String inpConvertedQuantity = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Converted Quantity'))

String inpConvertedUOM = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Converted UoM'))


String inpPriceRuleValue = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'PriceRuleValue'))

String inpPricingSlab = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Pricing Slab_Pricing type'))


String inpPricingSlab_GreaterThanQuantity = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Pricing Slab_Valid from quantity'))

String inpPricingSlab_StandardValue = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Pricing Slab_PriceStandardValue'))


String inpPerUOM = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Per UoM'))

String Condition_MarkupPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Condition_Markup'))




String Condition_MinPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Condition_Min Price'))

String Condition_MaxPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Condition_Max Price'))


String Amount_CalculatedPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Amount_Calculated Price'))

String Amount_MinPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Amount_Min Price'))

String Amount_MaxPrice = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Amount_Max Price'))

String Amount_SubTotal = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Amount Sub Total '))

String Amount_NetServiceCharge = ChargingOutput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ChargingOutputheader, 'Amount_NetServiceCharge'))


WebUI.delay(8)

WebUI.waitForElementVisible(findTestObject('PriceAnalysis/ChargingMenu'), 10)

WebUI.waitForElementClickable(findTestObject('PriceAnalysis/ChargingMenu'), 40000)

WebUI.click(findTestObject('PriceAnalysis/ChargingMenu'))

WebUI.delay(10)

not_run: WebUI.waitForElementVisible(findTestObject('Pricing/Page_ChargingDashboard/Toggle-Manual'), 10)

//WebUI.waitForElementClickable(findTestObject('Pricing/Page_ChargingDashboard/Toggle'), 40000)
not_run: WebUI.click(findTestObject('Pricing/Page_ChargingDashboard/Toggle-Manual'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('PriceAnalysis/Searchinput'), 'css')

WebUI.getText(findTestObject('PriceAnalysis/search'))

String ServiceRequestNumber = CustomKeywords.'googleSheets.GoogleSheets.getkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 
    'KeyValue!A2:B', '001-1ServiceRequestNumber_Charging')

WebUI.click(findTestObject('PriceAnalysis/Reset'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('PriceAnalysis/Searchinput'), ServiceRequestNumber)

WebUI.delay(5)

WebUI.click(findTestObject('PriceAnalysis/search'))

WebUI.delay(5)

//Validating Dashboard
String chargeType = WebUI.getText(findTestObject('PriceAnalysis/ChargeType'))

String checkQuantity = WebUI.getText(findTestObject('PriceAnalysis/Quantity'))

String invoicingStatus = WebUI.getText(findTestObject('PriceAnalysis/InvoicingStatus'))

String reasonCode = WebUI.getText(findTestObject('PriceAnalysis/ReasonCode'))

String invoiceCycle = WebUI.getText(findTestObject('PriceAnalysis/InvoiceCycle'))

if (chargeType.equals(ChargeType) == false) {
    KeywordUtil.markFailed((('Actual Charge Type ' + chargeType) + 'is not equal to expected charge type ') + ChargeType)
}

if (checkQuantity.equals(CheckQuantity + UOM) == false) {
    KeywordUtil.markFailed((('Actual Quantity ' + checkQuantity) + 'is not equal to expected quantity ') + (CheckQuantity + 
        UOM))
}

if (invoicingStatus.equals(InvoicingStatus) == false) {
    KeywordUtil.markFailed((('Invocing Status ' + invoicingStatus) + 'is not equal to expected Invocing status ') + InvoicingStatus)
}

if (reasonCode.equals(ReasonCode) == false) {
    KeywordUtil.markFailed((('Reason code ' + reasonCode) + 'is not equal to expected reason code ') + ReasonCode)
}

if (invoiceCycle.equals(InvoiceCycle) == false) {
    KeywordUtil.markFailed((('Invoice cycle ' + invoiceCycle) + 'is not equal to expected invoice cycle ') + InvoiceCyclenvoi)
}

WebUI.delay(6)

WebUI.click(findTestObject('PriceAnalysis/Pricingscreen'))

WebUI.delay(5)

String service = WebUI.getText(findTestObject('PriceAnalysis/Service'))

String chargeQuantity = WebUI.getText(findTestObject('PriceAnalysis/Charge_Quantity'))

String chargeUOM = WebUI.getText(findTestObject('PriceAnalysis/Charge_UOM'))

String pricingType = WebUI.getText(findTestObject('PriceAnalysis/PricingType'))

String convertedQuantity = WebUI.getText(findTestObject('PriceAnalysis/ConvertedQuantity'))

String convertedUOM = WebUI.getText(findTestObject('PriceAnalysis/ConvertedUOM'))

if ((inpPricingType == 'Standard') || (inpPricingType == 'Flat fee') || (inpPricingType == 'Mark up')) {
    String priceruleValue = WebUI.getText(findTestObject('PriceAnalysis/PriceRuleValue'))

    String perUOM = WebUI.getText(findTestObject('PriceAnalysis/PerUOM'))

    if (priceruleValue.trim().equals(inpPriceRuleValue.trim()) == false) {
        KeywordUtil.markFailed((('PriceRuleValue' + priceruleValue) + 'is not equal to expected PriceRuleValue ') + inpPriceRuleValue)
    }
    
    if (perUOM.equals(inpPerUOM) == false) {
        KeywordUtil.markFailed((('PerUOM' + perUOM) + 'is not equal to expected PerUOM ') + inpPerUOM)
    }
    
    if (perUOM.equals(inpPerUOM) == false) {
        KeywordUtil.markFailed((('PerUOM' + perUOM) + 'is not equal to expected PerUOM ') + inpPerUOM)
    }
}

if (inpPricingType == 'Tiers') {
    switch (inpChargeQuantity) {
        case '1.00':
            this.PricingSlabRow = 1

            break
        case '25.00':
            this.PricingSlabRow = 2

            break
        case '50.00':
            this.PricingSlabRow = 3
    }
    
    String pricingSlab = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab', [('PricingSlabRow') : this.PricingSlabRow]))

    String pricingSlab_GreaterThanQuantity = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab_GreaterThanQuantity', 
            [('PricingSlabRow') : this.PricingSlabRow]))

    String pricingSlab_PriceStandardValue = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab_PriceStandardValue', 
            [('PricingSlabRow') : this.PricingSlabRow]))

    if (pricingSlab.trim().equals(inpPricingSlab.trim()) == false) {
        KeywordUtil.markFailed((('PricingSlab' + pricingSlab) + 'is not equal to expected PricingSlab ') + inpPricingSlab)
    }
    
    if (pricingSlab_GreaterThanQuantity.trim().equals(inpPricingSlab_GreaterThanQuantity.trim()) == false) {
        KeywordUtil.markFailed((('PricingSlab_GreaterThanQuantity' + pricingSlab_GreaterThanQuantity) + 'is not equal to expected PricingSlab_GreaterThanQuantity ') + 
            inpPricingSlab_GreaterThanQuantity)
    }
    
    if (pricingSlab_PriceStandardValue.trim().equals(inpPricingSlab_StandardValue.trim()) == false) {
        KeywordUtil.markFailed((('PricingSlab_PriceStandardValue' + pricingSlab_PriceStandardValue) + 'is not equal to expected PricingSlab_PriceStandardValue ') + 
            inpPricingSlab_StandardValue)
    }
}


if (inpPricingType == 'Scales') {
	
	
	String pricingSlab = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab', [('PricingSlabRow') : 1]))

	String pricingSlab_GreaterThanQuantity = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab_GreaterThanQuantity',
			[('PricingSlabRow') : 1]))

	String pricingSlab_PriceStandardValue = WebUI.getText(findTestObject('PriceAnalysis/PricingSlab_PriceStandardValue',
			[('PricingSlabRow') : 1]))

	if (pricingSlab.trim().equals(inpPricingSlab.trim()) == false) {
		KeywordUtil.markFailed((('PricingSlab' + pricingSlab) + 'is not equal to expected PricingSlab ') + inpPricingSlab)
	}
	
	if (pricingSlab_GreaterThanQuantity.trim().equals(inpPricingSlab_GreaterThanQuantity.trim()) == false) {
		KeywordUtil.markFailed((('PricingSlab_GreaterThanQuantity' + pricingSlab_GreaterThanQuantity) + 'is not equal to expected PricingSlab_GreaterThanQuantity ') +
			inpPricingSlab_GreaterThanQuantity)
	}
	
	if (pricingSlab_PriceStandardValue.trim().equals(inpPricingSlab_StandardValue.trim()) == false) {
		KeywordUtil.markFailed((('PricingSlab_PriceStandardValue' + pricingSlab_PriceStandardValue) + 'is not equal to expected PricingSlab_PriceStandardValue ') +
			inpPricingSlab_StandardValue)
	}
}



String conditionMinPrice = WebUI.getText(findTestObject('PriceAnalysis/Condition', [('variable') : 'Min Price']))

String conditionMaxPrice = WebUI.getText(findTestObject('PriceAnalysis/Condition', [('variable') : 'Max Price']))

String amountCalculated = WebUI.getText(findTestObject('PriceAnalysis/Amount', [('variable') : 'Calculated Price']))

String amountMinPrice = WebUI.getText(findTestObject('PriceAnalysis/Amount', [('variable') : 'Min Price']))

String amountMaxPrice = WebUI.getText(findTestObject('PriceAnalysis/Amount', [('variable') : 'Max Price']))

String amountSubTotal = WebUI.getText(findTestObject('PriceAnalysis/Amount', [('variable') : 'Sub Total']))

String amountNetServiceCharge = WebUI.getText(findTestObject('PriceAnalysis/Amount', [('variable') : 'Net Service Charge']))

WebUI.scrollToPosition(9999999, 9999999)

if (service.equals(inpService) == false) {
    KeywordUtil.markFailed((('Service ' + service) + 'is not equal to expected Service ') + inpService)
}

if (chargeQuantity.equals(inpChargeQuantity) == false) {
    KeywordUtil.markFailed((('ChargeQuantity ' + chargeQuantity) + 'is not equal to expected ChargeQuantity ') + inpChargeQuantity)
}

if (chargeUOM.equals(inpChargeUOM) == false) {
    KeywordUtil.markFailed((('ChargeUOM ' + chargeUOM) + 'is not equal to expected ChargeUOM ') + inpChargeUOM)
}

if ((pricingType.trim()).equals((inpPricingType.trim())) == false) {
    KeywordUtil.markFailed((('PricingType' + pricingType) + 'is not equal to expected PricingType ') + inpPricingType)
}

if (convertedQuantity.equals(inpConvertedQuantity) == false) {
    KeywordUtil.markFailed((('ConvertedQuantity' + convertedQuantity) + 'is not equal to expected ConvertedQuantity ') + 
        inpConvertedQuantity)
}

if (convertedUOM.equals(inpConvertedUOM) == false) {
    KeywordUtil.markFailed((('ConvertedUOM' + convertedUOM) + 'is not equal to expected ConvertedUOM ') + inpConvertedUOM)
}

if (Condition_MinPrice.equals(conditionMinPrice) == false) {
    KeywordUtil.markFailed((('Actual Condition min price ' + conditionMinPrice) + 'is not equal to expected Condition min price ') + 
        Condition_MinPrice)
}

if (Condition_MaxPrice.equals(conditionMaxPrice) == false) {
    KeywordUtil.markFailed((('Actual Condition max price ' + conditionMaxPrice) + 'is not equal to expected Condition max price ') + 
        Condition_MaxPrice)
}

if (Amount_CalculatedPrice.equals(amountCalculated) == false) {
    KeywordUtil.markFailed((('Actual amount calculated ' + amountCalculated) + 'is not equal to expected amount calculated ') + 
        Amount_CalculatedPrice)
}

if (Amount_MinPrice.equals(amountMinPrice) == false) {
    KeywordUtil.markFailed((('Actual amount min price' + amountMinPrice) + 'is not equal to expected amount min price ') + 
        Amount_MinPrice)
}

if (Amount_MaxPrice.equals(amountMaxPrice) == false) {
    KeywordUtil.markFailed((('Actual amount max price' + amountMaxPrice) + 'is not equal to expected Amount max price ') + 
        Amount_MaxPrice)
}

if (Amount_SubTotal.equals(amountSubTotal) == false) {
    KeywordUtil.markFailed((('Actual amount subtotal' + amountSubTotal) + 'is not equal to expected amount sutotal ') + 
        Amount_SubTotal)
}

if (Amount_NetServiceCharge.equals(amountNetServiceCharge) == false) {
    KeywordUtil.markFailed((('Actual amount net service charge' + amountNetServiceCharge) + 'is not equal to expected Amount net service charge ') + 
        Amount_NetServiceCharge)
}

