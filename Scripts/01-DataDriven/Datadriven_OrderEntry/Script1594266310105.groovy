import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.sql.DriverManager as DriverManager
import java.sql.ResultSet as ResultSet
import java.sql.Statement as Statement
import com.kms.katalon.core.annotation.Keyword as Keyword
import java.text.SimpleDateFormat as Date
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
//WebUI.callTestCase(findTestCase('Login/login'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(5)

def intRow = 0

def introwstart = Integer.parseInt(Rowstart)

def introwend = Integer.parseInt(Rowend)

def rowcount = introwend - introwstart

System.out.println(Rowstart)

//WebUI.callTestCase(findTestCase('000-Login/00-LoginOrderManagement_AdministrativeOrder'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.navigateToUrl('https://ostest-irl-02.vopak.com/OrderMng/CSRDashboard.aspx')
def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnew

List<List> Mysysorderinput = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'(strGoogleSheetDDnew, (('MysOrdersinput!A' + 
    Rowstart) + ':AZ') + Rowend)

List<List> TransactionalData = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'(strGoogleSheetDDnew, (('TransactionalData!A' + 
    Rowstart) + ':AY') + Rowend)

List<List> headervalues = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strGoogleSheetDDnew, 
    'MysOrdersinput')



//environment
System.out.println(intRow)

System.out.println(rowcount)

//String strbaseTCname = Mysysorderinput.get(intRow).get(2)
def col = 0

List<List> headervaluestrans = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strGoogleSheetDDnew, 
    'TransactionalData')

System.out.println(headervaluestrans)

System.out.println (TransactionalData)

	while (intRow <= rowcount) {
    System.out.println(col)

    System.out.println(intRow)

    //A = CustomKeywords.'googleSheets.GoogleSheets.RowColNumberToBeReadOrUpdate'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'MysOrdersinput','test2', '03-2LoadTankTruckToleranceCheck', '1', '1','Read')
    String inpTerminalName = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Terminal'))

    //B
    String inpOrderLine = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Orderline'))

    String inpAdditionalNo = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'AdditionalNo'))

    //C
    String inpTestcasename = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'TestcaseName'))

    //D
    String inpTemplate = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'TemplateName'))

    //E
    String inpCustomer = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Customer'))

    //F
    String inpCustomerref = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Customerref'))

    //G
    String inpAgreement = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Agreement'))

    //H
    String inpProduct = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Customer Product'))

    //I
    String inpTank = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(headervalues, 
            'Tank'))

    //J
    String inpServicetemplate = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Servicetemplate'))

    //W
    String inpCallTransportName = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Callvisit_nameoftransport'))

    String inpServiceType = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Service Type'))

    String inpUnitType = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Unit Type'))

    String inpSampleSize = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Sample Size'))

    //Z
    //  boolean inpScaleTicketindicator = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(headervalues, 'ScaleTicketIndicator'))
    if ((inpOrderLine == '1') && (inpAdditionalNo == '0')) {
        GlobalVariable.TerminalName = Mysysorderinput.get(intRow).get(0)

        WebUI.callTestCase(findTestCase('000-Login/00-LoginOrderManagement_ProductmovementOrder'), [('TerminalName') : inpTerminalName], 
            FailureHandling.STOP_ON_FAILURE)

        WebUI.waitForElementClickable(findTestObject('Page_CSR Dashboard/Order Entry Menu'), 40000)

        WebUI.click(findTestObject('Page_CSR Dashboard/Order Entry Menu'))

        WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/OrderHeaderData_ProductMovement'), [('Customer') : inpCustomer
                , ('CustomerRef') : inpCustomerref, ('Agreement') : inpAgreement, ('Product') : inpProduct, ('Tank') : inpTank
                , ('ServiceTemplate') : inpServicetemplate], FailureHandling.STOP_ON_FAILURE)
    } else {
        if (inpAdditionalNo == '0') {
            WebUI.scrollToPosition(10, 10)

            WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/CreateOrder'))
        }
    }
    
    //empty assignment on initialize
    /*    if (strbaseTCname != inpTestcasename && intRow !=0) {
		
		WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Release Order'), [:], FailureHandling.STOP_ON_FAILURE)
        strbaseTCname = Mysysorderinput.get(intRow).get(2)
    } 
*/
    //In this switch it will select the correct template based the data driven set, 
    //it will login when the order line is set to one, else it will continue creating orders for a specific service request	
    switch (inpTemplate) {
		
		
        case 'LoadTankTruck':
		System.out.println (TransactionalData)
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/LoadTankTruck'), [('row') : intRow
                    , ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)

			

            System.out.println(intRow)

            intRow = intRow + 1

            break
        case 'ExternalLineTruck':
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/ExternallineTruck'), [('row') : intRow
                    , ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)
			
		     TransactionalData = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'(strGoogleSheetDDnew, (('TransactionalData!A' +
				Rowstart) + ':AY') + Rowend)
			
            System.out.println(intRow)
			
			System.out.println (TransactionalData)

            intRow = intRow + 1

            break
        case 'UnloadVesselorBarge':
          
		
	
			
			WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/UnloadVesselorBargenew'), [('row') : intRow
				, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)

            intRow = intRow + 1

            break
        case 'UnloadTankTruck':
		
		   WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/UnloadTankTruck'), [('row') : intRow
			, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)

        

            intRow = intRow + 1

            break
        case 'LoadVesselorBarge':
          
			
			
			WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/LoadVesselorBarge'), [('row') : intRow
				, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)

		
            intRow = intRow + 1

            break
			
		case 'LoadPipeline':
			
			 WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/LoadPipeline'), [('row') : intRow
				  , ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)
    		  intRow = intRow + 1
  
			  break
		
        case 'LoadRailcar':
     		WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/LoadRailCar'), [('row') : intRow
				, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)
            intRow = intRow + 1

            break
        case 'UnloadRailcar':
       
			
			WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/UnloadRailcar'), [('row') : intRow
				, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues,('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : headervaluestrans], FailureHandling.STOP_ON_FAILURE)
	

            intRow = intRow + 1

            break
        case 'Take_Sample_Tank_Pint':
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/OperationalExecution/Take_Sample_Tank_Pint'), 
                [('row') : intRow, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues], FailureHandling.STOP_ON_FAILURE)

            intRow = (intRow + 1)

            break
        case 'Take_Sample_RC_Pint':
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/OperationalExecution/Take_Sample_RC_Pint'), 
                [('row') : intRow, ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues], FailureHandling.STOP_ON_FAILURE)

            intRow = (intRow + 1)

            break
        case 'TankToTank':
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/TankTank'), [('row') : intRow
                    , ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues], FailureHandling.STOP_ON_FAILURE)

            intRow = (intRow + 1)

            break
        case 'Scale Ticket':
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/OperationalExecution/Scale_Ticket'), [('row') : intRow
                    , ('Mysysorderinput') : Mysysorderinput, ('ColumnHeaders') : headervalues], FailureHandling.STOP_ON_FAILURE)

            intRow = (intRow + 1)

            break
    }
    
    //C
    //check if it is the end of the error, else check the testcase name (if different release the total order) 
    System.out.println(rowcount)

    System.out.println(intRow)

    if (intRow > rowcount) {
        WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Release Order'), [:], FailureHandling.STOP_ON_FAILURE //create orderline
            )
    } else {
        String inpTestcasenamenewrow = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
                headervalues, 'TestcaseName'))

        if (inpTestcasename.equals(inpTestcasenamenewrow)) {
        } else {
            WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Release Order'), [:], FailureHandling.STOP_ON_FAILURE)
        }
    }
}

