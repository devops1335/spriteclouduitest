import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('Page_CSR Dashboard/Order Entry Menu'), 20)

//CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_CSR Dashboard/Order Entry Menu'), 20)

WebUI.waitForElementClickable(findTestObject('Page_CSR Dashboard/Order Entry Menu'), 40000)

WebUI.click(findTestObject('Page_CSR Dashboard/Order Entry Menu'))

WebUI.delay(3)

WebUI.getText(findTestObject('Page_Order Entry_Header/Customer'))


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/Customer'), 20000)

WebUI.click(findTestObject('Page_Order Entry_Header/Customer'))

WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(Customer, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/Agreement'), 5000)


WebUI.click(findTestObject('Page_Order Entry_Header/Agreement'))

WebUI.delay(2)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(Agreement, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/Product'), 5000)

WebUI.click(findTestObject('Page_Order Entry_Header/Product'))

WebUI.delay(2)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(Product, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/ServiceTemplate'), 5000)

WebUI.click(findTestObject('Page_Order Entry_Header/ServiceTemplate'))

WebUI.delay(3)


WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(ServiceTemplate, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.delay(3)


WebUI.click(findTestObject('Page_Order Entry_Header/CustomerRef'))

//WebUI.sendKeys(findTestObject('Page_Order Entry_Header/CustomerRef'), GlobalVariable.Version.toString())
WebUI.sendKeys(findTestObject('Page_Order Entry_Header/CustomerRef'), Keys.chord(CustomerRef, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order Entry_Header/ServiceTemplate'), Keys.chord(
        Keys.TAB))

WebUI.delay(2)


WebUI.takeScreenshot()
WebUI.click(findTestObject('Page_Order Entry_Header/Create'))

