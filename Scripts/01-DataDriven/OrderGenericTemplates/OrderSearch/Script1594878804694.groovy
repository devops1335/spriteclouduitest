import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

def OrderNumber = TransactionalData.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'OrderNumber'))

def ServiceRequestNumber = TransactionalData.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'ServiceRequestNumber'))

WebUI.sendKeys(findTestObject('Page_CSR Dashboard/SearchInput'), OrderNumber)

WebUI.delay(2)

WebUI.click(findTestObject('Page_CSR Dashboard/SearchButton'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_CSR Dashboard/ClickOnOrderNumber', [('value') : OrderNumber]))

WebUI.delay(2)

def AdditionalNo = TransactionalData.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'AdditionalNo'))

if (AdditionalNo != '0') {
 def strstate  =   WebUI.getAttribute(findTestObject('Page_CSR Dashboard/AdditionalState'), 'title')

    CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
        'TransactionalData', 'y', TestCaseName, OrderLineNo, strstate, 'Ms Orders State Service Request', ColumnHeaders)

   
	
}

if (AdditionalNo == '0') {
	
	def strstate  =   WebUI.getAttribute(findTestObject('Object Repository/Page_CSR Dashboard/MainorderState'), 'title')
    CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
        'TransactionalData', 'y', TestCaseName, OrderLineNo, strstate, 'Ms Orders State Service Request', ColumnHeaders)
}

