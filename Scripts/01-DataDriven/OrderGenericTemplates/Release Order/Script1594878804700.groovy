import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//WebUI.scrollToElement(findTestObject('Page_Order_Entry_line/ReleaseOrder'), 20)

WebUI.scrollToPosition(1410, 12)
WebUI.delay(2)
WebUI.waitForElementVisible(findTestObject('Page_Order_Entry_line/ReleaseOrder'), 20)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/ReleaseOrder'), 1000)

WebUI.delay(4)

WebUI.click(findTestObject('Page_Order_Entry_line/ReleaseOrder'), FailureHandling.STOP_ON_FAILURE)


  WebUI.delay(8)
  
  WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/ConfirmIFrame'), 5)
  
  
	/*
	 * WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/Confirm'),
	 * 10)
	 * 
	 * WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/Confirm')
	 * , 10)
	 * 
	 * WebUI.click(findTestObject('Page_Order_Entry_line/Confirm'),
	 * FailureHandling.STOP_ON_FAILURE)
	 * 
	 */
/*
 * WebUI.delay(3)
 * 
 * WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/
 * ConfirmOrderReadinessIFrame'), 5)
 * 
 * WebUI.delay(3)
 * 
 * WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/Confirm'),
 * 5)
 */


WebUI.waitForElementVisible(findTestObject('Page_Order_Entry_line/Confirm'), 20)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/Confirm'), 200)

WebUI.click(findTestObject('Page_Order_Entry_line/Confirm'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)


WebUI.takeScreenshot()

//WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/SwitchtoorderReleaseFrame'), 20)

//WebUI.delay(5)


//WebUI.verifyElementText(findTestObject('Page_Order_Entry_line/checkifreleased'), 'Released')

//WebUI.switchToDefaultContent()

//CustomKeywords.'utility.keywords.enterkeys.HoverandCLick'(findTestObject('Page_Call_Request/closemodalx'))

//WebUI.waitForElementClickable(findTestObject('Page_CSR Dashboard/Order Entry Menu'), 40000)

//WebUI.delay(8)