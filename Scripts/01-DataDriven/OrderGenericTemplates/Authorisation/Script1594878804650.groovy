import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


def AuthorizationMaxQuantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'AuthorizationMaxQuantity'))

def AuthorizationUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'AuthorizationUOM'))

def AuthorizationNoExecutions = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'AuthorizationNumberofExecutions'))

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/AuthorisationOrderCheck'))

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/AuthorisationOrderCheck'), 30)

WebUI.click(findTestObject('Page_Order_Entry_line/AuthorisationOrderCheck'))

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/NewAuthorisation'), 30)

WebUI.scrollToElement(findTestObject('Page_Order_Entry_line/NewAuthorisation'), 30)

WebUI.click(findTestObject('Page_Order_Entry_line/NewAuthorisation'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Page_Order_Entry_line/AuthOrderMaxQuantity'), 2)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/AuthOrderStartDate'), 30)

WebUI.delay(2)

not_run: WebUI.click(findTestObject('Page_Order_Entry_line/AuthOrderStartDate'))



WebUI.callTestCase(findTestCase('01-DataDriven/Generic/setDatepickerandTime'), [('TestobjectDate') : findTestObject('Page_Order_Entry_line/AuthOrderStartDate')
        , ('TestobjectTime') : findTestObject('Page_Order_Entry_line/AuthOrderStartTime'), ('addsubtractmin') : -10, ('addsubtracthour') : -2
        , ('addsubtractday') : -1, ('addsubtractmonth') : 0, ('addsubtractyear') : 0, ('TestobjectClickoffModal') : findTestObject(
            'Page_Order_Entry_line/AuthOrderMaxQuantity')], FailureHandling.STOP_ON_FAILURE)

//strDateTimenow = CustomKeywords.'datepicker.DatePicker.GetAttributesofDateTime'(findTestObject('Page_Order_Entry_line/AuthOrderStartDate'), findTestObject('Object Repository/Page_Order_Entry_line/AuthOrderStartTime'))
WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/AuthOrderEndDate'), 30)

WebUI.delay(3)

WebUI.callTestCase(findTestCase('01-DataDriven/Generic/setDatepickerandTime'), [('TestobjectDate') : findTestObject('Page_Order_Entry_line/AuthOrderEndDate')
        , ('TestobjectTime') : findTestObject('Page_Order_Entry_line/AuthOrderEndTime'), ('addsubtractmin') : 0, ('addsubtracthour') : 0
        , ('addsubtractday') : 7, ('addsubtractmonth') : 0, ('addsubtractyear') : 0, ('TestobjectClickoffModal') : findTestObject(
            'Page_Order_Entry_line/AuthOrderMaxQuantity')], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)


WebUI.sendKeys(findTestObject('Page_Order_Entry_line/AuthOrderMaxQuantity'), AuthorizationMaxQuantity)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/AuthOrderUOM'))

'lb selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : AuthorizationUOM]), 0)

'Variable lb\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : AuthorizationUOM]))

if (AuthorizationNoExecutions != 'N/A') {


    WebUI.sendKeys(findTestObject('Object Repository/Page_Order_Entry_line/AuthOrderNumberOfvisits'), AuthorizationNoExecutions)
}

