import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew

def BlendFinalProduct = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Blend Final Product'))

def BlendFinalProductName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'Blend Final ProductName'))

def UseBlendprogramfromTestcaseorCreate = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'UseBlendprogramfromTestcaseorCreate'))

WebUI.click(findTestObject('Page_Order_Entry_line/Click_BlendService'))

if (UseBlendprogramfromTestcaseorCreate == 'CREATE') {
    WebUI.delay(4)

    WebUI.click(findTestObject('BlendServiceRequest/NewBlend'))

    WebUI.delay(3)

    WebUI.click(findTestObject('BlendServiceRequest/BlendFinalProduct'))

    CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('BlendServiceRequest/BlendFinalProduct'), Keys.chord(
            BlendFinalProduct, Keys.ENTER))

    WebUI.delay(4)

    WebUI.click(findTestObject('BlendServiceRequest/BlendFinalProductName'))

    CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('BlendServiceRequest/BlendFinalProductName'), 
        Keys.chord(BlendFinalProductName, Keys.ENTER //*************************
            ) //use the blend program
        )
} else {
    WebUI.delay(4)

    WebUI.click(findTestObject('BlendServiceRequest/ExistingBlend'))

    WebUI.delay(3)

    def UseBlendprogramfromTestcaseOrderline = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            ColumnHeaders, 'UseBlendprogramfromTestcaseOrderline'))

    intline = Integer.parseInt(UseBlendprogramfromTestcaseOrderline)

    System.out.println(TransactionalData)
	
    def BlendProgram = TransactionalData.get(intline).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            ColumnHeadersTrans, 'BlendProgramnumber'))
   
    WebUI.click(findTestObject('BlendServiceRequest/checkradioBlend', [('value') : BlendProgram]))
	
	WebUI.delay(5)
}

