import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew
WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestSource'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestSource'))


WebUI.delay(3)

WebUI.switchToFrame(findTestObject('Page_Call_Request/CallReferenceIFrame'), 10)
WebUI.delay(2)

def inpCallvisitNameofTransport = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'Callvisit_nameoftransport'))


def inpCallForMultipleServiceRequests = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'CallForMultipleServiceRequests'))

def ServiceProviderOrganization = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'Callvisit_ServiceProvider'))


if (inpCallvisitNameofTransport != 'N/A')
{
	

	WebUI.sendKeys(findTestObject('Page_Call_Request/MeansofTransport'), inpCallvisitNameofTransport)
	WebUI.delay(3)

}

if (inpCallForMultipleServiceRequests == 'TRUE'){
	
	
	
}

if (ServiceProviderOrganization != 'N/A')
{

	WebUI.sendKeys(findTestObject('Page_Call_Request/CallOrganization'), inpCallvisitNameofTransport)
	WebUI.delay(3)
	
	WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)
	
	WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(ServiceProviderOrganization, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(3)

}


WebUI.click(findTestObject('Page_Call_Request/CallReference'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)


WebUI.waitForElementClickable(findTestObject('Page_Call_Request/SaveCallRequest'), 5000)

WebUI.click(findTestObject('Page_Call_Request/SaveCallRequest'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/EditCallRequestSource'))

WebUI.delay(3)


WebUI.delay(3)

WebUI.switchToFrame(findTestObject('Page_Call_Request/CallReferenceIFrame'), 10)
WebUI.delay(2)

def CallRequestNumber = WebUI.getText(findTestObject('Page_Call_Request/CallRequestReferenceNumber'))

//to improve later
CallRequestNumber = CallRequestNumber.substring(0, 4)


def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'TestcaseName'))

def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'Orderline'))

def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'AdditionalNo'))



CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew, 'TransactionalData', CallRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'CallRequestNumber', HeaderTrans)

WebUI.delay(4)

WebUI.switchToDefaultContent()

WebUI.delay(4)

WebUI.takeScreenshot()

CustomKeywords.'utility.keywords.enterkeys.HoverandCLick'(findTestObject('Page_Call_Request/closemodalx'))