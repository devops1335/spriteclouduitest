import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

//External line truck


WebUI.delay(4)
WebUI.click(findTestObject('Page_Order_Entry_line/SourcePipeline'))

WebUI.delay(4)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/DestinationTruck'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTruck'))

WebUI.delay(5)

def inptrucktype = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Truck Type'))

//utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/TruckType'))
//WebUI.click(findTestObject('Page_Order_Entry_line/TruckType'))
//CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/TruckType'), Keys.chord(
//       inptrucktype, Keys.ENTER))
not_run: CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/TruckType'), Keys.chord(
        Keys.ENTER))

WebUI.delay(3)

def inpSourceUnit = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Source'))

WebUI.click(findTestObject('Page_Order_Entry_line/SourceUnit_ExternalPipe'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceUnit_ExternalPipe'), 
    Keys.chord(inpSourceUnit, Keys.ENTER))






WebUI.delay(5)


def CountryOfOrigin = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'CountryOrigin'))

if (CountryOfOrigin != 'N/A')

{
	
	
	
	utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/CountryOfOrigin'))
WebUI.click(findTestObject('Page_Order_Entry_line/CountryOfOrigin'))



CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'), CountryOfOrigin)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'), Keys.chord(
		Keys.ENTER))


}


def inpDestinationplace = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
        ColumnHeaders, 'Destinationplace'))


WebUI.delay(5)

WebUI.click(findTestObject('Page_Order_Entry_line/PlaceofDestination'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        inpDestinationplace, Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        Keys.TAB))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'), 5000)


WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/CallDestination'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
        , ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)

def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Quantity'))

def QuanityUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'QuantityUOM'))

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.click(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

not_run: WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

'lb selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]), 0)

'Variable lb\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

def Tolerance = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Tolerance'))

if (Tolerance != 'NOCHECK') {
    def Toleranceminus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
            'Toleranceminus'))

    def Tolereranceplus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            ColumnHeaders, 'Tolereranceplus'))

    WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), ToleranceMinus)

    WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

    WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), TolerancePlus)

    WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)
}

//check if you have to create a blend 
def BlendIndicator = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'BlendIndicator'))

//blend part
if (BlendIndicator == 'TRUE') {

	
	WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Blend'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
		, ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(5)

def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'TestcaseName'))

def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Orderline'))

def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'AdditionalNo'))

def CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
    'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', ColumnHeadersTrans)

def OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
    'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', ColumnHeadersTrans)

def ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
    'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', ColumnHeadersTrans)

WebUI.delay(2)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

//save the blend program number in google sheets (can only be done after saving the order, so again validate)
if (BlendIndicator == 'TRUE') {
    def Blendnumber = WebUI.getText(findTestObject('Object Repository/Page_Order_Entry_line/BlendProgram'))

    def intvalue = Blendnumber.length() - 14

    def blendprogramnumber = Blendnumber.substring(Blendnumber.length() - intvalue)

    CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
        'TransactionalData', blendprogramnumber, TestCaseName, OrderLineNo, AdditionalNo, 'BlendProgramnumber', ColumnHeadersTrans)
}

//save the authorisation program number in google sheets (can only be done after saving the order, so again validate)
def Authorisationindicator

if (Authorisationindicator == 'TRUE') {
}

