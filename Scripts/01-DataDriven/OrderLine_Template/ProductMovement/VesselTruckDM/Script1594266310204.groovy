import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable



def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew
List<List> headervaluesTrans = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strGoogleSheetDDnew,
	'TransactionalData')


WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/SourceVessel'), 10000)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceVessel'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/DestinationTruck'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTruck'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'))

WebUI.callTestCase(findTestCase('null'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestSource'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestSource'))

WebUI.callTestCase(findTestCase('null'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.delay(3)

WebUI.click(findTestObject('Page_Order_Entry_line/CountryOfOrigin'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'),
	'United')

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'),
	Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Page_Order_Entry_line/Quantity'), 2)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), '60000')

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/UOM'), 'LB')

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/UOM'), Keys.chord(
		Keys.TAB))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), '5')

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), '5')

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/SaveOrder'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/ReleaseOrder'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/ConfirmIFrame'), 5)

WebUI.click(findTestObject('Page_Order_Entry_line/Confirm'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/ConfirmOrderReadinessIFrame'), 5)

WebUI.click(findTestObject('Page_Order_Entry_line/Confirm'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(15)

