import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
WebUI.delay(5)


def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew


WebUI.delay(2)


utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/DestinationTank'))

WebUI.delay(3)
WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/DestinationTank'), 10000)




WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTank'))



WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/SourceVessel'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceVessel'))

WebUI.delay(5)

//


WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTankCmb'))




def Tank = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Tank'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationTankCmb'), Keys.chord(
		Tank, Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationTankCmb'), Keys.chord(
		Keys.TAB))

WebUI.delay(4)



utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/CountryOfOrigin'))


WebUI.click(findTestObject('Page_Order_Entry_line/CountryOfOrigin'))




def CountryOfOrigin = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'CountryOrigin'))


CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'), CountryOfOrigin)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/CountryOfOrigin'), Keys.chord(
		Keys.ENTER))



	WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)
	
	def Tolerance = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Tolerance'))


	
	if (Tolerance != 'NOCHECK') {
		def Toleranceminus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
				ColumnHeaders, 'Toleranceminus'))

		def Tolereranceplus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
				ColumnHeaders, 'Tolereranceplus'))

		WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), ToleranceMinus)

		WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

		WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), TolerancePlus)

		WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)
	}
	

	
	
//check if to follow the authorisationorder path
def authorisationindicator = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
		ColumnHeaders, 'Authorisationorderindicator'))
if (authorisationindicator == 'TRUE') {
	
   
   
	 
	 WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Authorisation'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
			 , ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)
 
}
else {

	WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestSource'), 5000)
	
	System.out.println(TransactionalData)
	
		
	WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/CallSourcenew'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
			, ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(2)
	def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'Quantity'))

	utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

	WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

	WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

	
	
	WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

	WebUI.delay(2)

	
	
	def QuanityUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'QuantityUOM'))

	
	
	'lb selection\r\n'
	WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]), 60)
	
	WebUI.delay(2)
	'Variable lb\r\n'
	WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]))
}
	
	
//check if a blend needs to be created for the order
	def BlendIndicator = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'BlendIndicator'))

	if (BlendIndicator == 'TRUE') {
		
		
		WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Blend'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
			, ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)

		utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/SaveOrder'))
	}
	def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'TestcaseName'))

	def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'Orderline'))

	def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'AdditionalNo'))

	def CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)

	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
		'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', ColumnHeadersTrans)

	def OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)

	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
		'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', ColumnHeadersTrans)

	def ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))

	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
		'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', ColumnHeadersTrans)

	WebUI.delay(2)

	WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

	//save the blend program
	if (BlendIndicator == 'TRUE') {
		
		
		
		
		def Blendnumber = WebUI.getText(findTestObject('Object Repository/Page_Order_Entry_line/BlendProgram'))
		
		def intvalue = Blendnumber.length() - 14
		
		def blendprogramnumber = Blendnumber.substring(Blendnumber.length() - intvalue)
		
		CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew, 'TransactionalData', blendprogramnumber, TestCaseName, OrderLineNo, AdditionalNo, 'BlendProgramnumber', ColumnHeadersTrans)
		
		
		
	}
	
	
	//save the authorisation program
	def Authorisationindicator
	if (authorisationindicator == 'TRUE'){
		def authorisationnumber= WebUI.getText(findTestObject('Object Repository/Page_Order_Entry_line/AuthorisationProgram'))
		
		def intvalue = authorisationnumber.length() - 21
	
		def authorisationprogramnumber = authorisationnumber.substring(authorisationnumber.length() - intvalue)
		
		CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew, 'TransactionalData', authorisationprogramnumber, TestCaseName, OrderLineNo, AdditionalNo, 'AuthorisationProgramnumber', ColumnHeadersTrans)
		
	}