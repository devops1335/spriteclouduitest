import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys



def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnew
List<List> headervaluesTrans = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strGoogleSheetDDnew,
	'TransactionalData')


//integrate buysell part 
WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/DestinationTank'), 10000)

WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTank'))

WebUI.delay(3)



WebUI.click(findTestObject('Page_Order_Entry_line/DestinationTankCmb'))


def Destination = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Destination'))
CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationTankCmb'), Destination)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationTankCmb'), Keys.chord(
		Keys.TAB))

WebUI.delay(3)





def InternalTransferType = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'InternalTransferType'))


def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'TestcaseName'))

def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Orderline'))

def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'AdditionalNo'))


//first select source tank
WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/SourceTank'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceTank'))

WebUI.delay(5)

//then select internaltransfertype
if (InternalTransferType == "no"){
WebUI.click(findTestObject('Page_Order_Entry_line/Internaltransfertype'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/Internaltransfertype'),
	InternalTransferType)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/Internaltransfertype'),
	Keys.chord(Keys.ENTER))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)



//then select source tank name
WebUI.click(findTestObject('Page_Order_Entry_line/SourceTankcmb'))

def Source = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Source'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Source)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
		Keys.ENTER))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)


def CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', headervaluesTrans)
def OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', headervaluesTrans)
def ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', headervaluesTrans)


def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Quantity'))

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(5)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

WebUI.delay(4)

def QuanityUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'QuantityUOM'))

'lb selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]), 0)

'Variable lb\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuanityUOM]))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)


}
else {
//buysell part

WebUI.click(findTestObject('Page_Order_Entry_line/Internaltransfertype'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/Internaltransfertype'),
	'yes')

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/Internaltransfertype'),
	Keys.chord(Keys.ENTER))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByLabel(findTestObject('Page_Order_Entry_line/BUYSELL/PMisenteredforCUstomer'), 'Seller', false)

WebUI.delay(3)

WebUI.click(findTestObject('Page_Order_Entry_line/BUYSELL/BuyerAgreement'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/BUYSELL/BuyerAgreement'),
	'SVS50')

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/BUYSELL/BuyerAgreement'),
	Keys.chord(Keys.ENTER))

WebUI.delay(3)

not_run: WebUI.selectOptionByLabel(findTestObject('Page_Order_Entry_line/BUYSELL/Buyer'), 'Victory Tropical Oil (USA) inc.',
	false)

WebUI.click(findTestObject('Page_Order_Entry_line/BUYSELL/Buycustomerref'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/BUYSELL/Buycustomerref'),
	'test')

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/BUYSELL/Buycustomerref'),
	Keys.chord(Keys.ENTER))
}


WebUI.delay(3)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)
