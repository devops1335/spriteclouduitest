import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew


def Source = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Source'))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/SourceTank'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceTank'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceTankcmb'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Source)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
		Keys.ENTER))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/DestinationPipeline'), 5)

WebUI.click(findTestObject('Page_Order_Entry_line/DestinationPipeline'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_Order_Entry_line/DestinationPipelineCmb'))


def Destination = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Destination'))

WebUI.delay(5)


CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationPipelineCmb'),
	Destination)

WebUI.delay(5)


CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/DestinationPipelineCmb'),
	Keys.chord(Keys.ENTER))

WebUI.delay(5)



WebUI.click(findTestObject('Page_Order_Entry_line/PlaceofDestination'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Destination)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'),
	Keys.chord(Keys.ENTER))


def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Quantity'))

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)



def QuantityUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'QuantityUOM'))

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

'lb selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuantityUOM]), 0)

'Variable lb\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : QuantityUOM]))



//validating on storing
//check if a blend needs to be created for the order
def BlendIndicator = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'BlendIndicator'))

if (BlendIndicator == 'TRUE') {
	
	
	WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Blend'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
		, ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)

	utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/SaveOrder'))
}
def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'TestcaseName'))

def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Orderline'))

def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'AdditionalNo'))

def CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
	'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', ColumnHeadersTrans)

def OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
	'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', ColumnHeadersTrans)

def ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))

CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew,
	'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', ColumnHeadersTrans)

WebUI.delay(2)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

//save the blend program
if (BlendIndicator == 'TRUE') {
	
		
	
	def Blendnumber = WebUI.getText(findTestObject('Object Repository/Page_Order_Entry_line/BlendProgram'))
	
	def intvalue = Blendnumber.length() - 14
	
	def blendprogramnumber = Blendnumber.substring(Blendnumber.length() - intvalue)
	
	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'(strGoogleSheetDDnew, 'TransactionalData', blendprogramnumber, TestCaseName, OrderLineNo, AdditionalNo, 'BlendProgramnumber', ColumnHeadersTrans)
	
	
	
}
