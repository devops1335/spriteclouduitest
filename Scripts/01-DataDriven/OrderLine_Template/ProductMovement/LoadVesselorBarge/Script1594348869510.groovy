import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration



def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew




CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order_Entry_line/SourceTank'), 2000)
WebUI.waitForElementPresent(findTestObject('Page_Order_Entry_line/SourceTank'), 10000)
WebUI.delay(3)
WebUI.click(findTestObject('Page_Order_Entry_line/SourceTank'))
WebUI.delay(3)
CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order_Entry_line/DestinationVessel'), 2000)
WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/DestinationVessel'), 5000)
WebUI.click(findTestObject('Page_Order_Entry_line/DestinationVessel'))
WebUI.delay(4)


//
WebUI.click(findTestObject('Page_Order_Entry_line/SourceTankcmb'))




def Source = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders, 
        'Source'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
        Source, Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
        Keys.TAB))

WebUI.delay(4)



WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'), 5000)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/CallDestination'), [('row') : row, ('Mysysorderinput') : Mysysorderinput
	, ('ColumnHeaders') : ColumnHeaders, ('TransactionalData') : TransactionalData, ('ColumnHeadersTrans') : ColumnHeadersTrans], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Order_Entry_line/PlaceofDestination'))


def PlaceofDestination = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Destinationplace'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        PlaceofDestination, Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        Keys.TAB))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)





def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Quantity'))


utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

WebUI.delay(2)


def QuanityUOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'QuantityUOM'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/UOM'), Keys.chord(QuanityUOM, 
        Keys.ENTER))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/UOM'), Keys.chord(Keys.TAB))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)


def Tolerance = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'Tolerance'))

if (Tolerance != 'NOCHECK') {
def Toleranceminus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Toleranceminus'))

def Tolereranceplus = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
		ColumnHeaders, 'Tolereranceplus'))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), ToleranceMinus)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), TolerancePlus)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)
}

//check if you have to create a blend
def BlendIndicator = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	ColumnHeaders, 'BlendIndicator'))

//blend part
if (BlendIndicator == "TRUE") {
WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Blend'), [('UseBlendfrom') : ''], FailureHandling.STOP_ON_FAILURE)
}


//WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), ToleranceMinus)

//WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

//WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), TolerancePlus)

//WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)


def TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'TestcaseName'))

def OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Orderline'))

def AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'AdditionalNo'))


def CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)




CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', ColumnHeadersTrans)
def OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', ColumnHeadersTrans)
def ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', ColumnHeadersTrans)





WebUI.delay(2)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)



return OrderNumber

