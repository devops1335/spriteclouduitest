import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

not_run: CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Charging/servicetype'), 'xpath')

'Header '
WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Charging/servicetype'), 20000)

WebUI.waitForElementClickable(findTestObject('Charging/servicetype'), 20000)

WebUI.click(findTestObject('Charging/servicetype'))

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'), 
    'xpath')

WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(ServiceType, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('Charging/UnitType'), 20000)

WebUI.waitForElementClickable(findTestObject('Charging/UnitType'), 20000)

WebUI.click(findTestObject('Charging/UnitType'))

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'), 
    'xpath')

WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(UnitType, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

not_run: WebUI.selectOptionByIndex(findTestObject('Charging/UnitType'), 1)

WebUI.waitForElementVisible(findTestObject('Charging/UnitCall'), 20000)

WebUI.waitForElementClickable(findTestObject('Charging/UnitCall'), 20000)

WebUI.click(findTestObject('Charging/UnitCall'))

WebUI.click(findTestObject('Charging/Call'))

not_run: CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'), 
    'xpath')

not_run: WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

not_run: WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(DocumentType, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

'hr selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]), 0)

'Variable nbr\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/SaveOrder'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

def OrderNumber_Charging = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'googleSheets.GoogleSheets.UpdateAppendkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 'KeyValue', '001-OrderNumber_Charging', 
    OrderNumber_Charging)

def ServiceRequestNumber_Charging = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))

CustomKeywords.'googleSheets.GoogleSheets.UpdateAppendkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 'KeyValue', '001-1ServiceRequestNumber_Charging', 
    ServiceRequestNumber_Charging)

//WebUI.switchToFrame(findTestObject('Page_Order_Entry_line/ConfirmOrderReadinessIFrame'), 5)
//WebUI.click(findTestObject('Page_Order_Entry_line/Confirm'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(10)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Release Order'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

