import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


def strGoogleSheetDDnew = GlobalVariable.GoogleSheetOrderDDnewnew
List<List> headervaluesTrans = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strGoogleSheetDDnew,
	'TransactionalData')


def AdditionalNo
def TestCaseName
def OrderLineNo
def CustomerRef
def OrderNumber
def ServiceRequestNumber

AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
	'AdditionalNo'))

if (AdditionalNo == '0') {
	

	
	WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/Operational/Service Type'))
	
	CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order Entry_Header/inputandenter'), 20)
	
	CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'),
		'xpath')
	
	WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)
	
	def ServiceType = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Service Type'))
	
	WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(ServiceType, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

	WebUI.delay(5)
	
	def UnitType = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Unit Type'))
	

	WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/Operational/Unit Type'))
	
	
	CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order Entry_Header/inputandenter'), 20)
	
	CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'),
		'xpath')
	
	WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)
	
	WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(UnitType, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

	
		

	
		WebUI.delay(5)

	
	def UnitTank = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Tank'))
	
	WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/Operational/Unit TANK'))
	
	
	CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order Entry_Header/inputandenter'), 20)
	
	CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'),
		'xpath')
	
	WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)
	
	WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(UnitTank, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.delay(5)
	
	def samplesize = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Sample Size'))
	
	WebUI.click(findTestObject('Object Repository/Page_Order_Entry_line/Operational/Sample Size'))
	
	
	CustomKeywords.'utility.keywords.enterkeys.smartWait'(findTestObject('Page_Order Entry_Header/inputandenter'), 20)
	
	CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'),
		'xpath')
	
	WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)
	
	WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(samplesize, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)


	WebUI.delay(5)

	
	def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Quantity'))
	
	utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/Operational/QuantityOperational'))
	
	WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_Order_Entry_line/Operational/QuantityOperational'), Quantity)
	
	
//	WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))
	'lb selection\r\n'
//	WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]), 30)
	
	'Variable lb\r\n'
//	WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]))
	
	
	TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'TestcaseName'))
	
	OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'Orderline'))
	
	AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
			'AdditionalNo'))
	
	
	CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)
	
	
	
	
	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', headervaluesTrans)
	OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)
	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', headervaluesTrans)
	ServiceRequestNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/ServiceRequestID'))
	CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', headervaluesTrans)
	
	
	
	WebUI.waitForElementVisible(findTestObject('Page_Order_Entry_line/SaveOrder'), 20)
	
	WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/SaveOrder'), 20)
	
	WebUI.click(findTestObject('Page_Order_Entry_line/SaveOrder'), FailureHandling.STOP_ON_FAILURE)
	
} else {

WebUI.delay(4)

    WebUI.click(findTestObject('Page_Order_Entry_line/Additional/Checkbox', [('value') : 'Take Sample - Tank - Pint']))

    WebUI.delay(2)
	
	def Quantity = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Quantity'))

    WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Additional/Quanitity', [('value') : 'Take Sample - Tank - Pint']), Quantity)

    WebUI.delay(2)

   // WebUI.click(findTestObject('Page_Order_Entry_line/Additional/UOM', [('value') : 'Take Sample - Tank - Pint']))

    WebUI.delay(2)

	
//	def UOM = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		//'QuantityUOM'))
	
  //  'lb selection\r\n'
 //   WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]), 0)

 //   WebUI.delay(2)

    'Variable lb\r\n'
  //  WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]))

    WebUI.delay(2)

    WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(2)
	
	 TestCaseName = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'TestcaseName'))

 OrderLineNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'Orderline'))

AdditionalNo = Mysysorderinput.get(row).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(ColumnHeaders,
		'AdditionalNo'))


CustomerRef = WebUI.getText(findTestObject('Page_Order_Entry_line/CustomerRef'), FailureHandling.STOP_ON_FAILURE)




CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', CustomerRef, TestCaseName, OrderLineNo, AdditionalNo, 'CustomerRef', headervaluesTrans)
 OrderNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/OrderNumber'), FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', OrderNumber, TestCaseName, OrderLineNo, AdditionalNo, 'OrderNumber', headervaluesTrans)
 ServiceRequestNumber = WebUI.getText(findTestObject('Object Repository/Page_Order_Entry_line/Additional/additionalservicerequestid'))
CustomKeywords.'googleSheets.GoogleSheets.TransactionalTestDataToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'TransactionalData', ServiceRequestNumber, TestCaseName, OrderLineNo, AdditionalNo, 'ServiceRequestNumber', headervaluesTrans)






  	

}

