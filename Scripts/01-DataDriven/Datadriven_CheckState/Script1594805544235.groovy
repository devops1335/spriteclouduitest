import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoin
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.sql.DriverManager as DriverManager
import java.sql.ResultSet as ResultSet
import java.sql.Statement as Statement
import com.kms.katalon.core.annotation.Keyword as Keyword
import java.text.SimpleDateFormat as Date
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
//WebUI.callTestCase(findTestCase('Login/login'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(5)

def intRow = 0

def introwstart = Integer.parseInt(Rowstart)

def introwend = Integer.parseInt(Rowend)

def rowcount = introwend - introwstart

System.out.println(Rowstart)

//WebUI.callTestCase(findTestCase('000-Login/00-LoginOrderManagement_AdministrativeOrder'), [:], FailureHandling.STOP_ON_FAILURE)
//WebUI.navigateToUrl('https://ostest-irl-02.vopak.com/OrderMng/CSRDashboard.aspx')
def strGoogleSheetDD = GlobalVariable.GoogleSheetOrderDDnewnew

List<List> TransactionalData = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'(strGoogleSheetDD, (('TransactionalData!A' + 
    Rowstart) + ':AX') + Rowend)

//environment
System.out.println(intRow)

System.out.println(rowcount)

//String strbaseTCname = Mysysorderinput.get(intRow).get(2)
def col = 0

List<List> headervalues = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
    'TransactionalData')

while (intRow <= rowcount) {
    System.out.println(col)

    //A = CustomKeywords.'googleSheets.GoogleSheets.RowColNumberToBeReadOrUpdate'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'MysOrdersinput','test2', '03-2LoadTankTruckToleranceCheck', '1', '1','Read')
    String inpTerminalName = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Terminal'))

    //B
    String inpOrderLine = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'Orderline'))

    String inpAdditionalNo = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'AdditionalNo'))

    //C
    String inpTestcasename = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'TestcaseName'))

    String OrderNumber = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'OrderNumber'))

    String ServiceRequestNumber = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'OrderNumber'))

    //F
    String inpCustomerref = TransactionalData.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
            headervalues, 'CustomerRef'))

    //  boolean inpScaleTicketindicator = Mysysorderinput.get(intRow).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(headervalues, 'ScaleTicketIndicator'))
    WebUI.callTestCase(findTestCase('000-Login/00-LoginOrderManagement_ProductmovementOrder'), [('TerminalName') : inpTerminalName], 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/OrderSearch'), [('row') : 0, ('TransactionalData') : TransactionalData
            , ('ColumnHeaders') : headervalues], FailureHandling.STOP_ON_FAILURE)

    intRow = (intRow + 1)
}

