import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

//import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
WebUI.delay(3)

WebUI.switchToFrame(findTestObject('Page_Call_Request/CallReferenceIFrame'), 10)

WebUI.delay(3)

WebUI.delay(5)

WebUI.delay(5)

WebUI.click(findTestObject('Page_Call_Request/CallReference'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

if (MultipleServiceRequest == true) {
	
	WebUI.delay(2)
	
    WebUI.click(findTestObject('Page_Order_Entry_line/Multiple_Service_Request'))
}

WebUI.delay(4)

WebUI.waitForElementClickable(findTestObject('Page_Call_Request/SaveCallRequest'), 5000)

WebUI.click(findTestObject('Page_Call_Request/SaveCallRequest'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/EditCallRequestDestination'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('Page_Call_Request/CallReferenceIFrame'), 10)

WebUI.delay(3)

WebUI.delay(3)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Call_Request/CallReference'), FailureHandling.STOP_ON_FAILURE)

def CallRequestNumber = WebUI.getText(findTestObject('Page_Call_Request/CallRequestReferenceNumber'))

CustomKeywords.'googleSheets.GoogleSheets.UpdateAppendkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 'KeyValue', '03-2CallRequestNumber', 
    CallRequestNumber)

WebUI.delay(4)

WebUI.switchToDefaultContent()

WebUI.delay(4)

CustomKeywords.'utility.keywords.enterkeys.HoverandCLick'(findTestObject('Page_Call_Request/closemodalx'))

