import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

utilstatic.Util.ensureVisible(findTestObject('Page_Order_Entry_line/SaveOrder'))

WebUI.scrollToElement(findTestObject('Page_Order_Entry_line/SaveOrder'), 200)

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order_Entry_line/SaveOrder'), 'css')

WebUI.waitForElementVisible(findTestObject('Page_Order_Entry_line/SaveOrder'), 20)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/SaveOrder'), 20)

WebUI.click(findTestObject('Page_Order_Entry_line/SaveOrder'), FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order_Entry_line/Feedbackmessagewrapper'), 
    'css')

if (BlendIndicator == true) {
    def BlendProgramNumber = WebUI.getText(findTestObject('Page_Order_Entry_line/BlendProgram'), FailureHandling.STOP_ON_FAILURE)

    CustomKeywords.'googleSheets.GoogleSheets.UpdateAppendkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 'KeyValue', 
        'BlendProgramNumber', BlendProgramNumber)
}

WebUI.delay(5)

