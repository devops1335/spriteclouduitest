import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (Orderline=='1') {
	WebUI.click(findTestObject('BlendServiceRequest/Source_Tank'))
	

}
WebUI.click(findTestObject('BlendServiceRequest/Destination_Blend'))

WebUI.delay(4)

WebUI.click(findTestObject('Page_Order_Entry_line/TruckType'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/TruckType'), Keys.chord(
        TruckType, Keys.ENTER))

WebUI.delay(4)

WebUI.delay(3)

WebUI.click(findTestObject('Page_Order_Entry_line/Click_BlendService'))

WebUI.delay(4)

WebUI.click(findTestObject('Page_Order_Entry_line/SourceTankcmb'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
        'TK903', Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/SourceTankcmb'), Keys.chord(
        Keys.TAB))

WebUI.delay(4)

WebUI.waitForElementClickable(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'), 5000)

WebUI.click(findTestObject('Page_Order_Entry_line/CallVisit/NewCallrequestDestination'))

WebUI.callTestCase(findTestCase('sandbox/03-2-3CallRequestTruck'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/PlaceofDestination'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        PlaceofDestination, Keys.ENTER))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('Page_Order_Entry_line/PlaceofDestination'), Keys.chord(
        Keys.TAB))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/Quantity'))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

not_run: WebUI.sendKeys(findTestObject('Page_Order_Entry_line/Quantity'), Quantity)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Order_Entry_line/UOM'))

'lb selection\r\n'
WebUI.scrollToElement(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]), 0)

'Variable lb\r\n'
WebUI.click(findTestObject('Selectoptions/Selectoptionsbytext', [('value') : UOM]))

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/ToleranceMinus'), ToleranceMinus)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/TolerancePlus'), TolerancePlus)

WebUI.delay(4, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('BlendServiceRequest/NewBlend'))

WebUI.delay(3)

WebUI.click(findTestObject('BlendServiceRequest/BlendFinalProduct'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('BlendServiceRequest/BlendFinalProduct'), Keys.chord(
        BlendFinalProduct, Keys.ENTER))

WebUI.delay(3)

WebUI.click(findTestObject('BlendServiceRequest/BlendFinalProductName'))

CustomKeywords.'utility.keywords.enterkeys.clickAtOffset'(findTestObject('BlendServiceRequest/BlendFinalProduct'), Keys.chord(
        BlendFinalProductName, Keys.ENTER))

WebUI.delay(2)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

