import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//sheetname, tabname,nameofcolheader
//def columnindex = CustomKeywords.'googleSheets.GoogleSheets.GetIndexNumberofHeaderName'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 
//   'MysOrdersinput', 'Quantity UoM')
// CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowNo'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'MysOrdersinput','test2')
//def var = CustomKeywords.'googleSheets.GoogleSheets.RowColNumberToUpdateinSheet'('1nNYlRlnucPLVZckSCJhTWlzXo7aRWkiZM4Ieiq4bZxE', 'MysOrdersinput','test2', '03-2LoadTankTruckToleranceCheck', '1', '1')
System.out.println(headervalues)

WebUI.callTestCase(findTestCase('01-DataDriven/OrderLine_Template/ProductMovement/LoadRailCar'), [('TestcaseName') : '', ('OrderLineNo') : ''
        , ('AdditionalNo') : '', ('SourceUnit') : '', ('TruckType') : '', ('Destination') : '', ('PlaceofDestination') : ''
        , ('ToleranceMinus') : '', ('TolerancePlus') : '', ('Quantity') : '', ('UOM') : '', ('TestCaseName') : '', ('BlendIndicator') : false
        , ('BuySell') : false, ('OrderLine') : '', ('MultipleServiceRequest') : false, ('CallRequestNumber') : '', ('BlendProgramNumber') : ''
        , ('AdditionalService') : false, ('AdditionalQuantity') : '', ('ScaleTicketIndicator') : false], FailureHandling.STOP_ON_FAILURE)

