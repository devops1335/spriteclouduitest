import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('null'), [('TerminalName') : GlobalVariable.TerminalName], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.click(findTestObject('CancelOrder/FindOrder'))

Order = CustomKeywords.'googleSheets.GoogleSheets.getkeyvalue'(GlobalVariable.GoogleSheetKeyvalueName, 'KeyValue!A2:B', 
    '03-2OrderNumber')

WebUI.setText(findTestObject('CancelOrder/FindOrder'), Order)

WebUI.click(findTestObject('CancelOrder/SearchOrder'))

WebUI.click(findTestObject('CancelOrder/SelectOrder'))

WebUI.delay(4)

WebUI.click(findTestObject('CancelOrder/Cancel'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('CancelOrder/frame'), 4)

WebUI.waitForElementVisible(findTestObject('CancelOrder/ReasonForCancel'), 10)

WebUI.click(findTestObject('CancelOrder/ReasonForCancel'))

WebUI.sendKeys(findTestObject('CancelOrder/ReasonForCancel'), Keys.chord('Terminal Request', Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('CancelOrder/RequestedBy_DropDown'))

WebUI.sendKeys(findTestObject('CancelOrder/RequestedBy'), Keys.chord('LG', Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('CancelOrder/Confirm'), 0)

WebUI.click(findTestObject('CancelOrder/Confirm'))

WebUI.delay(2)

WebUI.verifyTextPresent('Last service request canceled. Order canceled as a result. Call / Visit is being used', false)

WebUI.closeBrowser()

