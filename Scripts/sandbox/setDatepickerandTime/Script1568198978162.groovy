import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//import java.text.DateFormat;
import java.util.Date as Date
import java.time.LocalDate as LocalDate
import java.time.LocalTime as LocalTime
import java.time.format.DateTimeFormatter as DateTimeFormatter

LocalDate idate

LocalTime itime

DateTimeFormatter dtfdate = DateTimeFormatter.ofPattern('yyyyMMdd')

DateTimeFormatter dtftime = DateTimeFormatter.ofPattern('HH:mm')

(idate, itime) = CustomKeywords.'datepicker.DatePicker.GetTimeZoneLocalDateTime'('America/New_York')

String strdatenow = idate.format(dtfdate)

String strtimenow = itime.format(dtftime)

String strdatenew = CustomKeywords.'datepicker.DatePicker.Setnewdate'(addsubtractday, addsubtractmonth, addsubtractyear, 
    idate)

String strtimenew = CustomKeywords.'datepicker.DatePicker.SetnewTime'(addsubtractmin, addsubtracthour, itime)

System.out.println(strdatenow)

System.out.println(strtimenow)

System.out.println(strdatenew)

System.out.println(strtimenew)

CustomKeywords.'datepicker.DatePicker.SetDatePickerDate'(TestobjectDate, strdatenow, strdatenew, TestobjectClickoffModal)

CustomKeywords.'datepicker.DatePicker.SetDatePickerTime'(TestobjectTime, strtimenew, TestobjectClickoffModal)



