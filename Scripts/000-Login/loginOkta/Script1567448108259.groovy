import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

//groovy 
String stros = System.getProperty('os.name')

if (stros.equals('Windows')) {
    WebUI.callTestCase(findTestCase('000-Login/loginOkta'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
}



strpasswords = '18oukae989dqvu_sIlxI4HyuRORZulJtDqzBK8bszFXM'

List<List> getpasswords = CustomKeywords.'googleSheets.GoogleSheets.getSpreadSheetRecords'(strpasswords, ('Password!A2:C6'))

List<List> getpasswordheadervalues = CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRow'(strpasswords,
	'Password')


//18oukae989dqvu_sIlxI4HyuRORZulJtDqzBK8bszFXM


//WebUI.openBrowser('https://vopak.okta-emea.com/')
WebUI.navigateToUrl('https://vopak.okta-emea.com/')

WebUI.delay(2)


String inpusername = getpasswords.get(0).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	getpasswordheadervalues, 'Username'))



WebUI.sendKeys(findTestObject('okta/oktaname'), inpusername)

WebUI.delay(2)

String inppassword = getpasswords.get(0).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	getpasswordheadervalues, 'Password'))


String otpkey = getpasswords.get(0).get(CustomKeywords.'googleSheets.GoogleSheets.GetHeaderRowColNumber'(
	getpasswordheadervalues, 'Secret key'))
//new password
WebUI.sendKeys(findTestObject('okta/password'), inppassword)

WebUI.click(findTestObject('okta/signin'))

WebUI.delay(2)

def strvalue

strvalue = CustomKeywords.'utility.keywords.ReadGoogleAuth.GetkeyValue'(otpkey)

WebUI.sendKeys(findTestObject('okta/codeverify'), strvalue)

WebUI.delay(2)

WebUI.click(findTestObject('okta/Verify'))

WebUI.delay(6)

