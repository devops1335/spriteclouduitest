import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Dimension as Dimension
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().window().setSize(new Dimension(1920, 1200))

WebUI.waitForJQueryLoad(10)

WebUI.navigateToUrl('ivopak.vopak.com')

WebUI.callTestCase(findTestCase('000-Login/loginOkta'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Url + '/OrderMng/CSRDashboard.aspx')

WebUI.delay(3)

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_CSR Dashboard/Terminal'), 'css')

WebUI.waitForElementVisible(findTestObject('Page_CSR Dashboard/Terminal'), 5000)

WebUI.waitForElementClickable(findTestObject('Page_CSR Dashboard/Terminal'), 5000)

WebUI.click(findTestObject('Page_CSR Dashboard/Terminal'))

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'), 
    'xpath')

WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord(GlobalVariable.TerminalName, Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

