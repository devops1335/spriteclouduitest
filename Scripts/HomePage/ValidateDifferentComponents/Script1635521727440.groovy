import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.ui.SystemOutputInterceptor
import internal.GlobalVariable

WebUI.openBrowser('')

WebUI.delay(5)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().window().setSize(new Dimension(1920, 1200))

driver.get(GlobalVariable.Url)

// Fetching all the components in a list of Web Elements
WebUI.waitForElementPresent(findTestObject('Object Repository/ComponentsValidation/ComponentName') , 10)

WebUI.waitForElementClickable(findTestObject('Object Repository/ComponentsValidation/ComponentName') , 10)

List<WebElement> componentsList = WebUI.findWebElements(findTestObject('Object Repository/ComponentsValidation/ComponentName'),10)

List<String> componentName= new ArrayList<String>()

// Saving the components name in a ArrayList

for (WebElement element : componentsList)
{
	componentName.add(element.getText())
	
}

System.out.println(componentName)


// Validating the components name

WebUI.verifyMatch(componentName.get(0), 'Elements', false)

WebUI.verifyMatch(componentName.get(1), 'Forms', false)

WebUI.verifyMatch(componentName.get(2), 'Alerts, Frame & Windows', false)
  
WebUI.verifyMatch(componentName.get(3), 'Widgets', false)
  
WebUI.verifyMatch(componentName.get(4), 'Interactions', false)
  
WebUI.verifyMatch(componentName.get(5), 'Book Store Application', false)
 




