import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order Entry_Header/inputandenter'), 
    'xpath')

WebUI.waitForElementClickable(findTestObject('Page_Order Entry_Header/inputandenter'), 5000)

WebUI.sendKeys(findTestObject('Page_Order Entry_Header/inputandenter'), Keys.chord('Administrative', Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

not_run: CustomKeywords.'office.Word.UpdateWordtextPicture'('D://Test.docx', 'This is the order header in which you type the following information', 
    'D:/', 'screenshotheader.jpg')

WebUI.delay(3)

CustomKeywords.'utility.keywords.waitfluent.waitForElementvisible'(findTestObject('Page_Order_Entry_line/servicetype'), 
    'css')

WebUI.click(findTestObject('Page_Order_Entry_line/servicetype'))

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/servicetype'), GlobalVariable.Version.toString())

WebUI.sendKeys(findTestObject('Page_Order_Entry_line/adminQuantiy'), '29')

WebUI.callTestCase(findTestCase('01-DataDriven/OrderGenericTemplates/Save_Order'), [:], FailureHandling.STOP_ON_FAILURE)

