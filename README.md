<h3>The purpose of this file to explain about SpriteCloudUITest (Katalon automation framework) and PetStore Test (Postman API collection): </h2>

<h2>SpriteCloudUITest:</h2>

<h4>a. Run the tests locally:</h4>

- Download the Katalon Studio on your desktop.
- Clone the project using using Gitlab URL "https://gitlab.com/devops1335/spriteclouduitest.git"
- Navigate to Test Suite, select the profile "Preprod" and click on Run button to run the Test Cases.



<h4>b. Run the test in a CI/CD pipeline:</h4>

- Login to Gitlab account
- Go to  project "https://gitlab.com/devops1335/spriteclouduitest.git".
- login to Gitlab account and navigate to CI/CD pipeline.
- Click on Run Pipeline to trigger the Tests.

<h4>c. Link to the results in Calliope.pro:</h4>

- Validate Components: https://app.calliope.pro/reports/105500/public/ff780e4f-afcc-49b3-ac5b-430e8c1d97c9
- ValidateBookStoreFlow: https://app.calliope.pro/reports/105499/public/38643769-8544-4a86-8cdb-609b13543579


<h4>d. Approach for selected scenarios:</h4>

1. Validate all the different components on the Home page: In this scenario, I am validating the different components text on the home page.
2. Validate the Book store application flow: In this scenario, I have covered the test cases like login functionality, "Add to collection" featre, alert pop up and added books on profile page.

 As a QA tester, it is necessary to figure out the business critical scenarios. Furthermore, I tried to ensure the maximum test coverage as possible.

<h4>e. Why are they the most important:</h4>

These scenarios covers the major critical fucntionality of the application, wherin I am validating the Book store application E2E flow.

<h4>f. What could be the next steps to your project:</h4>

- To integrate with Katalon TestOps for reporting purpose.
- To extend this framework to support Web Services Testing.
- To extend this framework to support Mobile app.
- Setting up of Katalon docker agent to run the Test suites.



<h2>PetStore API collection:</h2>

<h4>a. Run the tests locally:</h4>
- Download the Postman on your desktop.
- Import the collection via link : https://www.getpostman.com/collections/8c2dc4686adab23af7ea
- Run the E2E collection via runner.

<h4>b. Run the test in a CI/CD pipeline:</h4>

- Go to Jenkins server and navigate to created Job
- Click on "Build now" button to run the Job

This is configured on my local Jenkins server.  

<h4>c. Link to the results in Calliope.pro:</h4>

https://app.calliope.pro/reports/105498/public/cac2064c-e1aa-436f-82b2-72b90ed57f4d

<h4>d. Approach for selected scenarios:</h4>  

I created the workflow and chained API requests to automate the E2E flow of the application.

-  Scenario 1 : Add, Retrieve and update Pet
-  Scenario 2:  Place Order, Find Order, Check Inventory
-  Scenario 3:  Create Users, Retrieve Users and Login

<h4>e. Why are they the most important:</h4>

   These scenarios checking the business flow of the application and validating the API responses.

<h4>f. What could be the next steps to your project:</h4>

- To integrate postman collection with Gitlab as CI/CD
- These API's can be also be automated with RestAssured or Katalon Studio and can be used alon with UI tests
