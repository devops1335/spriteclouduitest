package datepicker

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import groovy.time.TimeCategory
import com.kms.katalon.core.logging.KeywordLogger

import java.text.SimpleDateFormat
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys as Keys
import java.text.DateFormat;
import java.util.Date;
import java.time.*
import java.time.format.DateTimeFormatter




import org.openqa.selenium.support.ui.ExpectedConditions


public class DatePicker {


	//add going out of datepicker to avoid stalereference exception

	@Keyword
	def SetDatePickerandTimeanddefineNewDateTime(TestObject objDate,TestObject objTime,int inp_minutes,int inp_hours,int inp_days,int inp_months,int inp_years,int int_offset,TestObject ClickNewElement){
		def driver = DriverFactory.getWebDriver()
		//get the time object attribute value
		WebElement element1 = WebUiCommonHelper.findWebElement(objTime,30)
		String strTimenow = element1.getAttribute("value")

		//Go to the date object click on datepicker to get the datenow attribute
		WebElement element2 = WebUiCommonHelper.findWebElement(objDate,30)
		System.out.println (element2.location)

		element2.click()


		// WebUiCommonHelper.wait.until(ExpectedConditions.visibilityOfElementLocated(findTestObject('Object Repository/CalendarPick/DateNow'), 30)
		WebElement element3 = driver.findElement(By.xpath("//table[@class='Calendar-topCont' and not (contains(@style, 'display'))]//descendant::div[contains(@class,'Calendar-day-today')]"))
		Thread.sleep(200)
		//	WebDriverWait wait = new WebDriverWait(driver,10);
		//	wait.until(ExpectedConditions.elementIfVisible(element3));
		//	element3.wait.until(ExpectedConditions.visibilityOfElementLocated(findTestObject('Object Repository/CalendarPick/DateNow'), 30)
		String strDatenow = element3.getAttribute("dyc-date")
		System.out.println(strDatenow)
		System.out.println(strTimenow)

		//concatenate  datenow and time now for the change time function
		String strDateTimenow = (strDatenow + ' ' + strTimenow)

		//set the new date, based on minutes, hours, days months and years,can be positive and negative
		String strDateTimenew = changedatetime(inp_minutes, inp_hours, inp_days, inp_months,  inp_years, int_offset, strDateTimenow)


		//substring the received datetime new
		String strDatenew = strDateTimenew.substring(0, 8)
		String strTimenew = strDateTimenew.substring(9, 14)



		System.out.println(strDateTimenew)

		System.out.println(strDatenow)

		System.out.println(strDatenew)

		System.out.println(strTimenew)

		System.out.println(strTimenow)

		//click the datepicker and set the date based on the new date, which is calculated from the date time now, timezone independent
		datepicker.DatePicker.handleDatepicker(strDatenow, strDatenew)

		//set the time based on the date time now, which is calculated timezone independent
		//WebUI.sendKeys(findTestObject(objTime), strTimenew)
		WebUI.delay(3)
		WebElement element4 = WebUiCommonHelper.findWebElement(objTime,30)

		//WebUI.sendKeys(findTestObject('Input'), Keys.chord('Text String',Keys.ENTER,Keys.TAB))
		element4.click()
		element4.sendKeys(Keys.chord(Keys.CONTROL,'a'))
		element4.sendKeys(strTimenew)

		Thread.sleep(5000)
		WebElement element5 = WebUiCommonHelper.findWebElement(ClickNewElement,30)
		element5.click()


		//(new utility.keywords.enterkeys()).clickAtOffset(objTime, strTimenew)
		//strDatenow = '20181226'
		//strTimenow = '05:32'




	}

	@Keyword
	def SetDatePickerTime(TestObject objTime, String strtimenew,TestObject ClickNewElement)
	{

		def driver = DriverFactory.getWebDriver()
		WebElement elementTime = WebUiCommonHelper.findWebElement(objTime,30)
		System.out.println (elementTime.location)
		elementTime.click()

		System.out.println (strtimenew)


		//set the time based on the date time now, which is calculated timezone independent
		//WebUI.sendKeys(findTestObject(objTime), strTimenew)
		WebUI.delay(3)
		WebElement element4 = WebUiCommonHelper.findWebElement(objTime,30)

		//WebUI.sendKeys(findTestObject('Input'), Keys.chord('Text String',Keys.ENTER,Keys.TAB))
		element4.click()
		WebUI.delay(2)

		String stros = System.getProperty("os.name")
		System.out.println(stros)
		//if (stros.equals("Mac OS X")) {
		//element4.sendKeys(Keys.chord(Keys.ARROW_RIGHT,Keys.ARROW_RIGHT,Keys.BACK_SPACE,Keys.BACK_SPACE,Keys.BACK_SPACE,Keys.BACK_SPACE,Keys.BACK_SPACE))

		WebUI.delay(2)
		//WebUI.setText(objTime, strtimenew)

		((JavascriptExecutor) driver).executeScript("arguments[0].value='" + strtimenew +  "'", elementTime)

		//	}

		//	else {


		//	Keys.chord(Keys.CONTROL, "a");
		//	element4.sendKeys(strtimenew)

		//		}




		//

		WebUI.delay(3)
		//element4.sendKeys(Keys.chord(Keys.TAB))

		WebElement element5 = WebUiCommonHelper.findWebElement(ClickNewElement,30)

		element5.click()


	}



	@Keyword
	def SetDatePickerDate(TestObject objDate,String strDatenow,String strDatenew, TestObject ClickNewElement){
		def driver = DriverFactory.getWebDriver()
		//Go to the date object click on datepicker to get the datenow attribute
		WebElement element2 = WebUiCommonHelper.findWebElement(objDate,30)
		System.out.println (element2.location)

		element2.click()





		//click the datepicker and set the date based on the new date, which is calculated from the date time now, timezone independent
		datepicker.DatePicker.handleDatepicker(strDatenow, strDatenew)
		WebUI.delay(3)
		//set the time based on the date time now, which is calculated timezone independent
		//WebUI.sendKeys(findTestObject(objTime), strTimenew)
		WebElement element5 = WebUiCommonHelper.findWebElement(ClickNewElement,30)


		element5.click()


		//(new utility.keywords.enterkeys()).clickAtOffset(objTime, strTimenew)
		//strDatenow = '20181226'
		//strTimenow = '05:32'




	}





	public static void handleDatepicker(String StrDatenow, String strDatenew)throws Exception{

		WebUI.click(findTestObject('Object Repository/CalendarPick/bottombartoday'))
		def strDatenow = StrDatenow
		String expDate = null, calYear = null,datepickerText=null,minYear=null,maxYear=null;
		int expMonth = 0, expYear = 0;
		WebElement datePicker;

		boolean yearnotfound = true;
		boolean monthnotfound = true;

		def driver = DriverFactory.getWebDriver()

		//define the condition criteria before the while loop
		String strYearnow = strDatenow.substring(0,4)
		System.out.println ("Yearnow" +  strYearnow)
		String strYearnew = strDatenew.substring(0,4)
		System.out.println ("YearNew" +  strYearnew)
		Integer intYearnew = Integer.parseInt(strYearnew)
		Integer intYearnow = Integer.parseInt(strYearnow)
		String strMonthnow = strDatenow.substring(4,6)
		String strMonthnew = strDatenew.substring(4,6)
		Integer intMonthnew = Integer.parseInt(strMonthnew)
		Integer intMonthnow = Integer.parseInt(strMonthnow)

		//check on the year not found
		while (yearnotfound) {
			//get the years


			if(intYearnow == intYearnew)
			{// Click on next button of date picker.

				yearnotfound = false;

			}
			else
			{
				if (intYearnow > intYearnew){


					System.out.println ("Yearnow" +  intYearnow)
					System.out.println ("YearNew" +  intYearnew)


					//WebElement PreviousYear =(driver).findElement(By.cssSelector('[class*="Calendar-prevYear"] div'));

					WebElement element = WebUiCommonHelper.findWebElement(findTestObject('CalendarPick/CalendarPreviousYear'), 30)
					element.click()
					intYearnow = intYearnow - 1
					//	WebDriverWait wait = new WebDriverWait(driver,10);
					//	wait.until(ExpectedConditions.visibilityOf(PreviousYear));
					//	PreviousYear.click()
				}
				else if (intYearnow < intYearnew){

					//System.out.println ("Yearnow" +  intYearnow)
					//System.out.println ("YearNew" +  intYearnew)
					WebElement element = WebUiCommonHelper.findWebElement(findTestObject('CalendarPick/Calendarnextyear'), 30)
					element.click()
					intYearnow = intYearnow + 1

				}
			}
		}
		//check on the month not found
		while (monthnotfound) {
			//get the years


			if(intMonthnow == intMonthnew)
			{// Click on next button of date picker.

				monthnotfound = false;

			}
			else
			{
				if (intMonthnow > intMonthnew){

					//System.out.println ("MonthNow" +  intMonthnow)
					//System.out.println ("MonthNew" +  intMonthnew)
					WebElement element = WebUiCommonHelper.findWebElement(findTestObject('CalendarPick/CalendarPreviousMonth'), 30)
					element.click()
					intMonthnow = intMonthnow - 1
				}
				else if (intMonthnow < intMonthnew){

					//System.out.println ("MonthNow" +  intMonthnow)
					//System.out.println ("MonthNew" +  intMonthnew)
					WebElement element = WebUiCommonHelper.findWebElement(findTestObject('CalendarPick/CalendarNextMonth'), 30)
					element.click()
					intMonthnow = intMonthnow + 1
				}

			}
		}

		//if two whiles are met we can click on the the button to select the newdate

		//WebElement ClickonNewDate = driver.findElement(By.xpath('//div[@dyc-date="' + strDatenew + '"]/ancestor::table[@class="Calendar-topCont" and not (contains(@style, "display"))]'))

		WebElement ClickonNewDate = driver.findElement(By.xpath('//table[@class="Calendar-topCont" and not (contains(@style, "display"))]//descendant::div[contains(@dyc-date,"' + strDatenew + '")]'))

		ClickonNewDate.click()

		////*[@dyc-date="20181228"]/ancestor::table[@class="Calendar-topCont" and not (contains(@style, "display"))]




	}


	@Keyword
	def GetoffsetTime() {

		Calendar caloffset = Calendar.getInstance();

		caloffset.setTimeZone(TimeZone.getTimeZone("America/New_York"));

		System.out.println("offset" + caloffset.get(Calendar.HOUR_OF_DAY) + ":"
				+ caloffset.get(Calendar.MINUTE));
		int offsethour = caloffset.get(Calendar.HOUR_OF_DAY)




	}

	@Keyword
	def GetoffSetdate() {

		Calendar caloffset = Calendar.getInstance();

		caloffset.setTimeZone(TimeZone.getTimeZone("America/New_York"));



		System.out.println("offset" + caloffset.get(Calendar.YEAR) + ":"
				+ caloffset.get(Calendar.MONTH) + caloffset.get(Calendar.DATE));
		int offsethour = caloffset.get(Calendar.HOUR_OF_DAY)




	}

	@Keyword getdatetime(String timezoneacountrycity) {

		//sets the datetime now
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm");
		Date datetime = new Date();
		sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		String format = sdf.format(datetime);

	}


	@Keyword
	def GetTimeZoneLocalDateTime(String timezoneCountryCity){

		ZoneId zoneid1 = ZoneId.of("America/New_York");
		LocalDate idDate = LocalDate.now(zoneid1)
		LocalTime idTime = LocalTime.now(zoneid1)
		return [idDate, idTime]
	}



	@Keyword
	def Setnewdate(long inp_DatefromToday,long inp_MonthfromToday,long inp_YearfromToday,LocalDate idDate){



		//this method uses the timecategory to calcute the new date time, time zone independent, based on Datetimenow

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");

		LocalDate idDatedays = idDate.plusDays(inp_DatefromToday)
		LocalDate idDateMonths = idDatedays.plusMonths(inp_MonthfromToday)
		String idDateDayMonthYears = idDateMonths.plusYears(inp_YearfromToday).format(dtf)
		System.out.println(idDateDayMonthYears)




		return idDateDayMonthYears

	}

	@Keyword
	def SetnewTime(long inp_MinutefromToday,long inp_HourfromToday,LocalTime idTime){



		//this method uses the timecategory to calcute the new date time, time zone independent, based on Datetimenow


		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");

		LocalTime idDatetime = idTime.plusMinutes(inp_MinutefromToday)
		String idDatehourMin = idDatetime.plusHours(inp_HourfromToday).format(dtf)

		System.out.println(idDatehourMin)




		return idDatehourMin

	}










}








