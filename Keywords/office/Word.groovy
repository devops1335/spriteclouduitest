package office

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords


/*import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI*/


import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.BreakType;


class Word {



	@Keyword
	def CreateWord(String filename){
		//Blank Document
		XWPFDocument document = new XWPFDocument();

		//Write the Document in file system
		FileOutputStream out = new FileOutputStream( new File(filename));

		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();


		document.write(out);

		out.close();
		System.out.println("createdocument.docx written successully");
		//Get lines from text file




	}





	@Keyword
	def UpdateWordtextPicture(String filename,String testscripttext, String filelocactionScreenshot,String screenshot_name){


		//provide the filename in this structure ("d:/xyz/doc1.rtf")

		XWPFDocument docx = new XWPFDocument(new FileInputStream(filename));
		XWPFParagraph paragraph = docx.createParagraph();
		XWPFRun run = paragraph.createRun();
		//String screenshot_name = System.currentTimeMillis() + ".png";
		//BufferedImage image = new Robot()
		//	.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		//ImageIO.write(image, "png", new File("d:/xyz/" + screenshot_name));
		InputStream pic = new FileInputStream(filelocactionScreenshot + screenshot_name);
		//docx.addPictureData(pic, Document.PICTURE_TYPE_PNG);
		run.addBreak();
		run.setText(testscripttext)
		run.addPicture(pic, XWPFDocument.PICTURE_TYPE_JPEG, screenshot_name,Units.toEMU(350), Units.toEMU(350));
		pic.close();
		FileOutputStream out = new FileOutputStream(filename);
		docx.write(out);
		//out.flush();
		out.close();


	}
}