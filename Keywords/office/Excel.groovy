package office

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class Excel {


	private static int findRow(XSSFSheet sheet, String cellContent) {
		for (Row row : sheet) {
			for (Cell cell : row) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
						return row.getRowNum();
					}
				}
			}
		}
		return 0;
	}

	@Keyword
	public void writeKeyvalue(String name,String Value,String strExcelLocation, String GetSheetName) throws IOException{
		// eg sheetname "MyOrderData"  and ExcelLocation "D:\\Excel\\test.xlsx"


		FileInputStream fis = new FileInputStream(strExcelLocation)


		// Do stuff you need to do with a file that is NOT locked.
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet(GetSheetName);

		int rownr=0
		rownr = findRow(sheet, name);


		if (rownr==0) {

			int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
			rownr = rowCount + 1;

		}
		Row row = sheet.createRow(rownr);
		Cell cell = row.createCell(0);
		cell.setCellType(cell.CELL_TYPE_STRING);
		cell.setCellValue(name);
		Cell cell1 = row.createCell(1);
		cell1.setCellType(cell1.CELL_TYPE_STRING);
		cell1.setCellValue(Value);
		FileOutputStream fos = new FileOutputStream(strExcelLocation);
		workbook.write(fos);
		fos.close();





	}

	@Keyword
	public void writescreenshot(String name,String Value,String strExcelLocation, String GetSheetName) throws IOException{
		// eg sheetname "MyOrderData"  and ExcelLocation "D:\\Excel\\test.xlsx"


		FileInputStream fis = new FileInputStream(strExcelLocation)


		// Do stuff you need to do with a file that is NOT locked.
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet(GetSheetName);

		int rownr=0
		rownr = findRow(sheet, name);






		if (rownr==0) {

			int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
			rownr = rowCount + 1;

		}

		//	BufferedImage image = ImageIO.read(new File("D:/poi.png"));
		//	ByteArrayOutputStream baps = new ByteArrayOutputStream();
		//	ImageIO.write(image,"png",baps);

		// int pictureIdx = wwb.addPicture(baps.toByteArray(), Workbook.PICTURE_TYPE_PNG);

		// XSSFDrawing drawing = ws.createDrawingPatriarch();
		//XSSFCreationHelper helper = wwb.getCreationHelper();
		//XSSFClientAnchor anchor = helper.createClientAnchor();
		//anchor.setCol1(1);
		//anchor.setRow1(1);

		// Picture picture = drawing.createPicture(anchor, pictureIdx);
		//picture.resize();


		Row row = sheet.createRow(rownr);
		Cell cell = row.createCell(0);
		cell.setCellType(cell.CELL_TYPE_STRING);
		cell.setCellValue(name);
		Cell cell1 = row.createCell(1);
		cell1.setCellType(cell1.CELL_TYPE_STRING);
		cell1.setCellValue(Value);
		FileOutputStream fos = new FileOutputStream(strExcelLocation);
		workbook.write(fos);
		fos.close();





	}






	@Keyword
	public String ReadKey(String name, String strExcelLocation,String GetSheetName) throws IOException{
		FileInputStream fis = new FileInputStream(strExcelLocation)

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet(GetSheetName);



		int rownr=0
		int colnr = 1
		rownr = findRow(sheet, name);


		Row row = sheet.getRow(rownr);
		Cell cell = row.getCell(colnr);


		return cell
	}
}
