package utilstatic

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.WebDriverWait
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import java.util.concurrent.TimeUnit



public class Util {

	public static void jsClick(TestObject testObject) {
		WebElement element = WebUiCommonHelper.findWebElement(testObject, 30);
		WebDriver driver = DriverFactory.getWebDriver();
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		jsExecutor.executeScript("arguments[0].click();", element);
	}


	public static void updatevalue(TestObject testObject){
		WebElement element = WebUiCommonHelper.findWebElement(testObject, 30);
		WebDriver driver = DriverFactory.getWebDriver();
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		jsExecutor.executeScript("arguments[0].setAttribute('contentEditable', 'True')", element);

		jsExecutor.executeScript("arguments[0].setAttribute('value', '2018-12-20')", element);
	}
	public static GetTestdataworkspacefolder(){



		def TestDataprojectdir = RunConfiguration.getProjectDir() + '\\TestData\\'
		return TestDataprojectdir
		def Imagesprojectdir = RunConfiguration.getProjectDir() + '\\Images\\'
	}
	public static GetImagesworkspacefolder(){



		def TestDataprojectdir = RunConfiguration.getProjectDir() + '\\TestData\\'
		return TestDataprojectdir
		def Imagesprojectdir = RunConfiguration.getProjectDir() + '\\Images\\'
	}

	public static def ensureVisible(TestObject testObject) {
		WebElement elt = WebUiCommonHelper.findWebElement(testObject, 20)
		def eltTop = elt.location.y
		def eltHeight = WebUI.getElementHeight(testObject)
		def eltBottom = eltTop + eltHeight

		def viewportTop = WebUI.getViewportTopPosition()
		def viewportBottom = viewportTop + WebUI.getViewportHeight()
		def lineTop = viewportTop  + 150

		if ( eltTop > viewportBottom || eltBottom < lineTop) {
			//	Log.i("Scrolling element into view. (${eltTop}-${eltBottom} outside range: ${lineTop}-${viewportBottom}")
			int offset=viewportBottom - lineTop
			if ( offset < 100 ) {
				offset = 100
			}
			int scrollPos = eltTop - offset
			if ( scrollPos < 0 ) {
				scrollPos = 0
			}
			WebUI.scrollToPosition(0, scrollPos)
		}}

	public static String getLeftString(String st,int length){
		int stringlength=st.length();

		if(stringlength<=length){
			return st;
		}

		return st.substring((stringlength-length));
	}


}

