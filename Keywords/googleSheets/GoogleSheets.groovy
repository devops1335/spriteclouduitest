package googleSheets

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.*;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;


import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.api.services.sheets.v4.model.*;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import internal.GlobalVariable

public class GoogleSheets {


	private static final String SERVICE_ACCOUNT_EMAIL = "katalon@spreadsheetreader-224412.iam.gserviceaccount.com";

	/** Path to the Service Account's Private Key file */
	private static final String SERVICE_ACCOUNT_JSON_FILE_PATH = System.getProperty("user.dir") + "/SpreadsheetReader-bd7c93cf5196.json";




	public static Sheets getSheetsService() {
		final HttpTransport TRANSPORT = new NetHttpTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();

		System.setProperty("https.proxyHost", "proxy.vopak.com");
		System.setProperty("https.proxyPort", "8080");
		System.setProperty("http.proxyHost", "proxy.vopak.com");
		System.setProperty("http.proxyPort", "8080");


		System.out.println(SERVICE_ACCOUNT_JSON_FILE_PATH)


		java.io.File jsonKey = new java.io.File(SERVICE_ACCOUNT_JSON_FILE_PATH);
		InputStream inputStream = new FileInputStream(jsonKey);
		GoogleCredential credential =
				GoogleCredential.fromStream(inputStream,TRANSPORT, JSON_FACTORY)

		credential = credential.createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS));



		Sheets service = new Sheets.Builder(TRANSPORT, JSON_FACTORY, null).setApplicationName("Katalondrive")
				.setHttpRequestInitializer(credential).build();
		return service;
	}

	@Keyword
	def List<List<Object>> getSpreadSheetRecords(String spreadsheetId, String range) throws IOException {
		Sheets service = getSheetsService();
		ValueRange response = service.spreadsheets().values()

				.get(spreadsheetId, range)
				.execute();
		List<List<Object>> values = response.getValues();
		if (values != null && values.size() != 0) {
			return values;
		} else {
			System.out.println("No data found.");
			return null;
		}
	}


	@Keyword
	def String getCellvalue(String spreadsheetId, String range) throws IOException{



		// How values should be represented in the output.
		// The default render option is ValueRenderOption.FORMATTED_VALUE.
		String valueRenderOption = ""; // TODO: Update placeholder value.

		// How dates, times, and durations should be represented in the output.
		// This is ignored if value_render_option is
		// FORMATTED_VALUE.
		// The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
		String dateTimeRenderOption = ""; // TODO: Update placeholder value.

		Sheets sheetsService = getSheetsService();
		Sheets.Spreadsheets.Values.Get request =
				sheetsService.spreadsheets().values().get(spreadsheetId, range);
		//request.setValueRenderOption(valueRenderOption);
		//request.setDateTimeRenderOption(dateTimeRenderOption);

		ValueRange response = request.execute();
		String strresponse = response.values.get(0).get(0)
		return strresponse
		// TODO: Change code below to process the `response` object:
		System.out.println(strresponse);
	}


	@Keyword
	def String getkeyvalue(String spreadsheetId, String rangeid,String selectvalue) throws IOException {
		Sheets service = getSheetsService();

		def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues().size() + 1

		String range  = rangeid + totalcount

		ValueRange response = service.spreadsheets().values()

				.get(spreadsheetId, range)
				.execute();
		List<List<Object>> values = response.getValues();
		String rangeToUpdate;
		String selectedBatchID = selectvalue
		System.out.println("all values in range: " + values.toString());


		int i = 0;
		if (values != null) {
			for (List row : values) {
				i += 1;
				if (row.get(0).equals(selectvalue)) {
					System.out.println("IT'S A MATCH! i= " + (i+1));
					rangeToUpdate = "A" + (i + 1) + ":B" + (i + 1); //row to be updated
					return row.get(1)

				}
			}

			System.out.println("IT'S not A MATCH!" );
		}
	}


	@Keyword
	def String UpdateAppendkeyvalue(String spreadsheetId, String rangeid,String key,String keyvalue) throws IOException {
		Sheets service = getSheetsService();

		def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues().size() + 1

		String range  = rangeid + '!A2:B' + totalcount

		ValueRange response = service.spreadsheets().values()

				.get(spreadsheetId, range)
				.execute();
		List<List<Object>> values = response.getValues();
		String rangeToUpdate;

		System.out.println("all values in range: " + values.toString());


		int i = 0;
		if (values != null) {
			for (List row : values) {
				i += 1;
				if (row.get(0).equals(key)) {
					System.out.println("IT'S A MATCH! i= " + (i+1));
					rangeToUpdate = rangeid + "!A" + (i + 1) + ":B" + (i + 1); //row to be updated
					UpdateSpreadSheetkeyvalueRecords(spreadsheetId,rangeToUpdate, key,keyvalue)

					return rangeToUpdate

				}
			}

			System.out.println("IT'S not A MATCH!" );
			AppendSpreadSheetkeyvalueRecords(spreadsheetId,rangeid, key,keyvalue)
			rangeToUpdate = "A" + (i + 2) + ":B" + (i + 2); //row to be updated
			return rangeToUpdate

		}
	}


	@Keyword
	def String UpdateAppendTestCasevalue(String spreadsheetId, String rangeid,String Application,String TestcaseDescription, String ExpectedResult,String Screenshotlink,String resultok, String Urllink) throws IOException {
		Sheets service = getSheetsService();

		def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues().size() +1

		String range  = rangeid + '!A1:G' + totalcount

		ValueRange response = service.spreadsheets().values()

				.get(spreadsheetId, range)
				.execute();
		List<List<Object>> values = response.getValues();
		String rangeToUpdate;

		System.out.println("all values in rangee: " + values.toString());


		int i = 0;
		if (values != null) {
			for (List row : values) {
				i += 1;
				if (row.get(1).equals(TestcaseDescription)) {
					System.out.println("IT'S A MATCH! i= " + (i));
					rangeToUpdate = rangeid + "!A" + (i) + ":G" + (i); //row to be updated
					String strtestcase = "TC-" + (i-1)
					UpdateSpreadSheetTestCasevalueRecords(spreadsheetId,rangeToUpdate, strtestcase,TestcaseDescription,Application,ExpectedResult, Screenshotlink,resultok,Urllink)

					return rangeToUpdate

				}
			}

			System.out.println("IT'S not A MATCH!" );
			int j
			j = i
			String strtestcase = "TC-" + (j)
			AppendSpreadSheetTestCasevalueRecords(spreadsheetId,rangeid, strtestcase,TestcaseDescription,Application,ExpectedResult, Screenshotlink,resultok,Urllink)
			rangeToUpdate = rangeid + "!A" + (i + 2) + ":G" + (i + 2); //row to be updated
			return rangeToUpdate

		}
	}





	@Keyword
	def String TransactionalTestDataToUpdateinSheet (String spreadsheetId, String rangeid,String SelectTestDatavalue,String selectTestcaseName, String Orderline,String Additional,String ColumnName, List<String> headervalues) throws IOException {

		def num = GetHeaderRowColNumber(headervalues, ColumnName)
		def RowNumber= RowNumbertobeRead(spreadsheetId,rangeid,selectTestcaseName,Orderline,Additional)
		def strcolletter = ''
		int quot
		int rem
		char letter
		String strletter
		int colnumber = num as Integer
		if (colnumber > 26) {

			colnumber = colnumber -26
			quot = colnumber/26;
			rem = colnumber%26;
			letter = (char)((int)'A' + rem);

			strletter = 'A' + letter
		}
		else if (colnumber > 52)	{

			colnumber = colnumber -52
			quot = colnumber/26;
			rem = colnumber%26;
			letter = (char)((int)'A' + rem);

			strletter = 'B' + letter
		}
		else if (colnumber <= 26){

			quot = colnumber/26;
			rem = colnumber%26;
			strletter = (char)((int)'A' + rem);
		}

		//   System.out.println strletter + RowNumber

		//now update the value
		def range = rangeid + '!' + strletter + RowNumber


		//getCellvalue(spreadsheetId,range)
		UpdateSpreadSheetRecords(spreadsheetId,range,SelectTestDatavalue)




	}



	@Keyword
	def List<String> GetHeaderRow(String spreadsheetId, String rangeid) throws IOException {
		Sheets service = getSheetsService();

		//	List<String> header = service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues()[0]
		//def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues()[0]


		Sheets.Spreadsheets.Values.Get request =
				sheetsService.spreadsheets().values().get(spreadsheetId, rangeid);

		ValueRange response = request.execute();

		List<String> headervalues = response.getValues()[0]



		return headervalues


	}

	@Keyword
	def int GetHeaderRowColNumber(List<String> ColumnList,String ColumnHeaderName) throws IOException {

		def col = 0
		for (String elementheader : ColumnList) {

			if (ColumnHeaderName.equals(elementheader)){

				System.out.println(elementheader);
				System.out.println(col);
				return col
			}
			col++
		}
	}

	@Keyword
	def String ColumnHeaderNametobeRead(String spreadsheetId, String rangeid,String ColumnHeader, String TestcaseName, String Orderline, String Additional) throws IOException {
		Sheets service = getSheetsService();

		def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues().size() + 1


		Sheets.Spreadsheets.Values.Get request =
				sheetsService.spreadsheets().values().get(spreadsheetId, rangeid);

		ValueRange response = request.execute();
		List<List<String>> values = response.getValues();


		String rowToupdate;
		System.out.println("all values in range: " + values.toString());

		def row = 0
		def col = 0
		for (List<String> element : values) {
			col = 0
			for (Object subElement : element) {

				if (ColumnHeader.equals(subElement)){

					System.out.println(subElement);
					System.out.println(col);
				}
				else if (TestcaseName.equals(values.get(row).get(0)))   {

					System.out.println("Wrong value" + subElement)
				}
				col++
			}
			row++
		}
	}



	@Keyword
	def String RowNumbertobeRead(String spreadsheetId, String rangeid,String selectvalue,String Orderline,String Additional) throws IOException {
		Sheets service = getSheetsService();

		def totalcount =  service.spreadsheets().values().get(spreadsheetId, rangeid).execute().getValues().size() + 1


		Sheets.Spreadsheets.Values.Get request =
				sheetsService.spreadsheets().values().get(spreadsheetId, rangeid);

		ValueRange response = request.execute();
		List<List<String>> values = response.getValues();


		String rowToupdate;
		System.out.println("all values in range: " + values.toString());


		def listvaluessize = values.size



		int i = 1;
		if (values != null){
			for (List row : values) {



				def inprowlineconcat = selectvalue + Orderline + Additional


				def validationrowlineconcat = values.get(i).get(0) + values.get(i).get(1) + values.get(i).get(2)
				System.out.println(inprowlineconcat + "  Inputdata" );
				System.out.println(validationrowlineconcat + "  Outputdata" );
				System.out.println(i + "iterator")
				if (validationrowlineconcat.equals(inprowlineconcat)) {



					System.out.println("IT'S A MATCH! " + i);
					def rowid = i + 1
					return rowid
				}

				if (listvaluessize.equals(i+1)){

					System.out.println("IT'S not A MATCH!" );
					return "not a match"
				}

				i++
			}
		}
	}

	@Keyword
	def UpdateSpreadSheetRecords(String spreadsheetId, String range, String Stringvalue) throws IOException {
		Sheets service = getSheetsService();

		ValueRange body = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(Stringvalue)));
		UpdateValuesResponse result = sheetsService.spreadsheets().values()
				.update(spreadsheetId, range , body)
				.setValueInputOption("RAW")
				.execute();

		System.out.println(result)
	}


	@Keyword
	def AppendSpreadSheetRecords(String spreadsheetId, String range, String Stringvalue) throws IOException {
		Sheets service = getSheetsService();

		ValueRange body = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(Stringvalue)));

		AppendValuesResponse result = sheetsService.spreadsheets().values()
				.append(spreadsheetId, range , body)
				.setValueInputOption("USER_ENTERED")
				.setIncludeValuesInResponse(true)
				.execute();


		System.out.println(result)
	}



	@Keyword
	def UpdateSpreadSheetkeyvalueRecords(String spreadsheetId, String range, String key, String Stringvalue) throws IOException {
		Sheets service = getSheetsService();

		ValueRange body = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(key, Stringvalue)));
		UpdateValuesResponse result = sheetsService.spreadsheets().values()
				.update(spreadsheetId, range , body)
				.setValueInputOption("RAW")
				.execute();
	}

	@Keyword
	def AppendSpreadSheetkeyvalueRecords(String spreadsheetId, String rangevalue, String key,String Stringvalue) throws IOException {
		Sheets service = getSheetsService();

		ValueRange Appendbody = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(key, Stringvalue)));
		AppendValuesResponse result = sheetsService.spreadsheets().values()
				.append(spreadsheetId, rangevalue , Appendbody)
				.setValueInputOption("USER_ENTERED")
				.setIncludeValuesInResponse(true)
				.execute();
	}



	@Keyword
	def UpdateSpreadSheetTestCasevalueRecords(String spreadsheetId, String range, String Testcase,String TestcaseDescription,String Application, String ExpectedResult,String Screenshot,String Resultok,String UrlLink) throws IOException {
		Sheets service = getSheetsService();

		ValueRange body = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(Testcase, TestcaseDescription, Application, ExpectedResult, Screenshot,Resultok, UrlLink)));
		UpdateValuesResponse result = sheetsService.spreadsheets().values()
				.update(spreadsheetId, range , body)
				.setValueInputOption("RAW")
				.execute();
	}

	@Keyword
	def AppendSpreadSheetTestCasevalueRecords(String spreadsheetId, String rangevalue, String Testcase,String TestcaseDescription,String Application, String ExpectedResult,String Screenshot,String Resultok,String UrlLink) throws IOException {
		Sheets service = getSheetsService();

		ValueRange Appendbody = new ValueRange()
				.setValues(Arrays.asList(
				Arrays.asList(Testcase, TestcaseDescription, Application, ExpectedResult, Screenshot,Resultok, UrlLink)));
		AppendValuesResponse result = sheetsService.spreadsheets().values()
				.append(spreadsheetId, rangevalue , Appendbody)
				.setValueInputOption("USER_ENTERED")
				.setIncludeValuesInResponse(true)
				.execute();
	}






	@Keyword
	def AddSheetToExistingSpreadSheetReport(String spreadsheetId) {
		Sheets service = getSheetsService();
		List<Request> requests = new ArrayList<>();
		requests.add(new Request().setAddSheet(new AddSheetRequest()
				.setProperties(new SheetProperties()
				.setTitle("scstc")))

				);

		BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);

		BatchUpdateSpreadsheetResponse response = service.spreadsheets().batchUpdate(spreadsheetId, body).execute();
	}
}

