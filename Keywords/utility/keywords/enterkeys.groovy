package utility.keywords

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory


/*import MobileBuiltInKeywords as Mobile
 import WSBuiltInKeywords as WS
 import WebUiBuiltInKeywords as WebUI*/

public class enterkeys {


	@Keyword
	def clickAtOffset(TestObject to, String keyvalue) {

		def ele = WebUiCommonHelper.findWebElement(to, 30)
		WebDriver driver = DriverFactory.getWebDriver()
		Actions build = new Actions(driver)
		build.sendKeys(keyvalue).build().perform()
	}


	@Keyword
	def HoverandCLick(TestObject to) {

		def ele = WebUiCommonHelper.findWebElement(to, 30)
		WebDriver driver = DriverFactory.getWebDriver()


		Actions myActions = new Actions(driver)
		myActions.clickAndHold(ele).build().perform()
		myActions.moveToElement(ele).click().build().perform()
	}


	@Keyword
	def ClickonCalendar(TestObject to) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement element = WebUiCommonHelper.findWebElement(findTestObject(to))
		JavascriptExecutor executor= ((driver) as JavascriptExecutor)
		WebUI.executeJavaScript("arguments[0].click()",element)
	}

	@Keyword
	def smartWait(TestObject testObject, Integer waitTime){
		for (int j = 0; j <= waitTime; j++) {
			if (WebUI.verifyElementVisible(testObject, FailureHandling.OPTIONAL)){

				WebUI.comment('Testobject ' + ' is ready')
				break

				//continue
			}else{

				WebUI.comment('Element ' + testObject + ' is not ready.')
				WebUI.delay(1)
			}
			if(j==10){
				KeywordUtil.markFailed('Element ' + testObject + ' was never found')
				break
			}
		}
	}
}
