package utility.keywords



import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords


import java.text.SimpleDateFormat
/*import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI*/
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.WebDriver
import groovy.time.TimeCategory
import com.kms.katalon.core.logging.KeywordLogger
import java.util.Date



public class ChangeDates {
	private def Today



	@Keyword
	def changedatetime(int inp_MinutefromToday, int inp_HourfromToday, int inp_DatefromToday, int inp_MonthfromToday,int inp_YearfromToday,String strDatenowfromDatePicker){
		String newDate = strDatenowfromDatePicker
		def Todayd = new SimpleDateFormat("yyyyMMdd HH:mm")
		Date Today =  Todayd.parse(newDate)

		use (groovy.time.TimeCategory){
			//	KeywordLogger log = new KeywordLogger()
			//def today = input.parse("MM-dd-yyyy", startTime)
			Date out_DatefromToday = Today


			out_DatefromToday = Today + inp_MinutefromToday.minutes + inp_HourfromToday.hours + inp_DatefromToday.days + inp_MonthfromToday.months + inp_YearfromToday.years
			System.out.println(out_DatefromToday)
			String Currentdate = out_DatefromToday.toString()
			def Currentdateparse = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",Locale.ENGLISH)
			def CurrentDateAdjusted = Currentdateparse.parse(Currentdate)
			System.out.println(CurrentDateAdjusted)
			def sdff = new SimpleDateFormat("yyyyMMdd HH:mm")
			def frmDate = sdff.format(CurrentDateAdjusted); // Handle the ParseException here



			return frmDate

		}
	}
}






