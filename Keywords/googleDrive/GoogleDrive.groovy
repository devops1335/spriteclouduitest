package googleDrive

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.*;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;


import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.google.api.services.drive.model.FileList;
import java.util.List
import java.io.IOException

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

import java.util.Arrays;

import org.eclipse.core.runtime.IProgressMonitor;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.ByteArrayContent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;






public class GoogleDrive {




	private static final String SERVICE_ACCOUNT_EMAIL = "katalon@spreadsheetreader-224412.iam.gserviceaccount.com";

	/** Path to the Service Account's Private Key file */
	private static final String SERVICE_ACCOUNT_JSON_FILE_PATH = System.getProperty("user.dir") + "/SpreadsheetReader-bd7c93cf5196.json";




	public static Drive getDriveService() {
		final HttpTransport TRANSPORT = new NetHttpTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();


		System.setProperty("https.proxyHost", "proxy.vopak.com");
		System.setProperty("https.proxyPort", "8080");
		System.setProperty("http.proxyHost", "proxy.vopak.com");
		System.setProperty("http.proxyPort", "8080");
		System.out.println(SERVICE_ACCOUNT_JSON_FILE_PATH)


		java.io.File jsonKey = new java.io.File(SERVICE_ACCOUNT_JSON_FILE_PATH);
		InputStream inputStream = new FileInputStream(jsonKey);
		GoogleCredential credential =
				GoogleCredential.fromStream(inputStream,TRANSPORT, JSON_FACTORY)

		credential = credential.createScoped(Collections.singleton(DriveScopes.DRIVE));


		Drive service = new Drive.Builder(TRANSPORT, JSON_FACTORY, null).setApplicationName("Katalondrive")
				.setHttpRequestInitializer(credential).build();
		return service;
	}

	@Keyword
	def getFilelist(){

		Drive service = getDriveService()

		FileList result = service.files().list()
				.setPageSize(10)
				.setFields("nextPageToken, files(id, name)")
				.execute();
		List<File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			System.out.println("No files found.");
		} else {
			System.out.println("Files:");
			for (File file : files) {

				System.out.printf("%s (%s)\n", file.getName(), file.getId());
				//Permission permission = new Permission()
				//permission.Role = "owner"
				//permission.Type = "user"
				//permission.EmailAddress = "ralph.van.der.horst@vopak.com"

				//			PermissionList permissions = service.permissions().list(file.getId()).execute();
				//		    List<Permission> permit = permissions.getPermissions().Id()


				updatePermission(service,file.getId(),"15076410169405982076","owner")

			}
		}
	}



	private static Permission updatePermission(Drive service, String fileId,
			String permissionId, String newRole) {
		try {
			// First retrieve the permission from the API.
			Permission permission = service.permissions().get(
					fileId, permissionId).execute();
			permission.setRole(newRole);
			return service.permissions().update(
					fileId, permissionId, permission).execute();
		} catch (IOException e) {
			System.out.println("An error occurred: " + e);
		}
		return null;
	}

	@Keyword
	def uploadscreenshotbufferedfile(String strFileName){
		def driver = DriverFactory.getWebDriver()
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		out.write(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES));
		String folderId = "1QmdiFM54XHJPFacXoBwSyBf9g5ZvB_ra";
		File fileMetadata = new File();



		// use out

		fileMetadata.setName(strFileName);
		//fileMetadata.setName("RegressionTest 4.48");
		fileMetadata.setParents(Collections.singletonList(folderId));
		//fileMetadata.setMimeType("application/vnd.google-apps.photo");
		fileMetadata.setTeamDriveId("0ANbW5LYoQcorUk9PVA")
		//fileMetadata.setParents(Collections.singletonList(folderId))
		def imagebincontent = new ByteArrayContent("image/jpeg",out.toByteArray())

		File file = driveService.files().create(fileMetadata, imagebincontent)

				.setFields("id")
				.setSupportsTeamDrives(true)
				.execute();




		file.setWebViewLink()

		String fileID = ("File ID: " + file.getId());



		return file.getId();





	}

	@Keyword
	def uploadfile(String strFileName){



		//	file = driveservice.files().insert(body, new InputStreamContent(mimeType,
		//		new ByteArrayInputStream(filename))).execute();
		String folderId = "1r_h_nQUj0FUjf6xuX0fmK9zZbpUrDLFk";
		File fileMetadata = new File();

		fileMetadata.setName(strFileName);
		fileMetadata.setParents(Collections.singletonList(folderId))

		java.io.File filePath = new java.io.File(System.getProperty("user.dir")+ strFileName);
		FileContent mediaContent = new FileContent("image/png", filePath);
		File file = driveService.files().create(fileMetadata, mediaContent)
				.setFields("id,webViewLink")

				.execute();
		file.setWebViewLink()

		System.out.println ("File ID: " + file.webViewLink());



		return file.webViewLink();
	}


	@Keyword
	def createresultfolder() {

		//put in the team drive folder you want to store it
		String folderId = "1rFArix5lf_JGLpL7CVkocGpL7zzk2Fuc";
		File fileMetadata = new File();
		fileMetadata.setName("RegressionTest 4.48");
		fileMetadata.setParents(Collections.singletonList(folderId));
		fileMetadata.setMimeType("application/vnd.google-apps.folder");
		fileMetadata.setTeamDriveId("0ANbW5LYoQcorUk9PVA")
		File file = driveService.files().create(fileMetadata)
				.setFields("id")
				.setSupportsTeamDrives(true).execute();
		System.out.println("Folder ID: " + file.getId());
	}

	@Keyword
	def createsheetfile(String FolderID,String Name){

		Drive service = getDriveService()
		String folderId = FolderID ;
		File fileMetadata = new File();
		fileMetadata.setName("RegressionTest 4.50 test1");
		fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");
		fileMetadata.setTeamDriveId("0ANbW5LYoQcorUk9PVA")
		fileMetadata.setParents(Collections.singletonList(folderId))
		File file = driveService.files().create(fileMetadata)
				.setFields("id")
				.setSupportsTeamDrives(true).execute();
		System.out.println("File ID: " + file.getId());
		return file.getId();
	}

	@Keyword
	def permission(String fileid){
		Drive service = getDriveService()

		Permission permission = new Permission()
				.setType("anyone")
				.setRole("writer")

		service.permissions().create(fileid, permission)
	}
}