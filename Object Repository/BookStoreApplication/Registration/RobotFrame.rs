<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RobotFrame</name>
   <tag></tag>
   <elementGuidId>d994e0f5-264c-4509-bff3-966fcb98011c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'a-dzpt1icwmhad']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[title=&quot;reCAPTCHA&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>a-dzpt1icwmhad</value>
   </webElementProperties>
</WebElementEntity>
