<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddCollection</name>
   <tag></tag>
   <elementGuidId>b98f6fc3-092f-469c-a074-b6a4db8b1085</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addNewRecordButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='Add To Your Collection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addNewRecordButton</value>
   </webElementProperties>
</WebElementEntity>
