<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Profile</name>
   <tag></tag>
   <elementGuidId>6b37dee6-60b2-4a5e-b5e4-60526b6f3412</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addNewRecordButton']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[text()='Profile']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addNewRecordButton</value>
   </webElementProperties>
</WebElementEntity>
